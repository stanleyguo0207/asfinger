################################################################################
## @brief
#			email     stanleyguo0207@163.com
#     github    https://github.com/stanleyguo0207
#     gitee     https://gitee.com/stanleyguo0207
#
################################################################################

################################################################################
## @brief
#			增加windows平台定义
#
#     @param _WIN32_WINNT 含义如下
#     ...没有列出
#     #define _WIN32_WINNT_WIN7                   0x0601 // Windows 7
#     #define _WIN32_WINNT_WIN8                   0x0602 // Windows 8
#     #define _WIN32_WINNT_WINBLUE                0x0603 // Windows 8.1
#     #define _WIN32_WINNT_WINTHRESHOLD           0x0A00 // Windows 10
#     #define _WIN32_WINNT_WIN10                  0x0A00 // Windows 10
#
add_definitions(-D_WIN32_WINNT=0x0601)  # >= win7
#
#     @param WIN32_LEAN_AND_MEAN
#     具体含义就是当项目中包含#include<windows.h>时去除一些头文件的包含
add_definitions(-DWIN32_LEAN_AND_MEAN)  # 包含尽量精简的内容
#
#     @param
#     为了禁用Visual C++中的 min/max宏定义，可以在包含<windows.h>头文件之前加上：NOMINMAX
#     准库在<algorithm>头中定义了两个模板函数std::min() 和 std::max()。
#     可惜在 Visual C++ 无法使用它们，因为没有定义这些函数模板。
#     原因是名字min和max与<windows.h>中传统的min/max宏定义有冲突。
#     为了解决这个问题，Visual C++ 定义了另外两个功能相同的模板：_cpp_min() 和 _cpp_max()。
#     我们可以用它们来代替std::min() 和 std::max()
add_definitions(-DNOMINMAX)             # 去掉min/max的定义
#
################################################################################

################################################################################
## @brief
#			编译器配置
#
#     msvc
#
if (CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  message(STATUS "Begin loading MSVC configuration")
  # include(${CMAKE_SOURCE_DIR}/cmake/compiler/msvc/settings.cmake)
#
#     mingw
elseif (CMAKE_CXX_PLATFORM_ID STREQUAL "MinGW")
  message(STATUS "Begin loading MinGW configuration")
  include(${CMAKE_SOURCE_DIR}/cmake/compiler/mingw/settings.cmake)
#
#     clang 暂时不选clang作为编译器
elseif (CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
  message(STATUS "Begin loading Clang configuration")
  # include(${CMAKE_SOURCE_DIR}/cmake/compiler/clang/settings.cmake)
endif()
#
################################################################################