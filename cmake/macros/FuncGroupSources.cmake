################################################################################
## @brief
#			email     stanleyguo0207@163.com
#			github    https://github.com/stanleyguo0207
#			gitee     https://gitee.com/stanleyguo0207
#
#  @note
#     该文件参考 TrinityCore
#
################################################################################

################################################################################
## @brief
#			函数
#     根据参数，构建源码树
#     @name   GroupSources
#     @param  dir                   目录
#
macro(GroupSources dir)
  # WITH_SOURCE_TREE 源码树选项如果没有设置 跳过该操作
  if (NOT ${WITH_SOURCE_TREE} STREQUAL "")
    # 包含所有的头文件与c/c++源文件
    file(GLOB_RECURSE elements RELATIVE ${dir} *.h *.hpp *.c *.cpp *.cc)

    foreach(element ${elements})
      # 提取文件名和目录
      get_filename_component(element_name ${element} NAME)
      get_filename_component(element_dir ${element} DIRECTORY)

      if (NOT ${element_dir} STREQUAL "")
        # 如果子文件是个目录，将其作为源码的组
        if (${WITH_SOURCE_TREE} STREQUAL "flat")
          # 使用第一个子目录构建平面结构
          string(FIND ${element_dir} "/" delemiter_pos)
          if (NOT ${delemiter_pos} EQUAL -1)
            string(SUBSTRING ${element_dir} 0 ${delemiter_pos} group_name)
            source_group("${group_name}" FILES ${dir}/${element})
          else()
            # 建立层级结构
            # 文件位于根目录
            source_group("${element_dir}" FILES ${dir}/${element})
          endif()
        else()
          # 使用完整的层次来构建源码
          string(REPLACE "/" "\\" group_name ${element_dir})
          source_group("${group_name}" FILES ${dir}/${element})
        endif()
      else()
        # 如果文件位于根目录，将其放置在根目录组中
        source_group("\\" FILES ${dir}/${element})
      endif()
    endforeach()
  endif()
endmacro()
#
################################################################################

################################################################################
## @brief
#     使用 FOLDER target属性将目标组织到文件夹中。
#     如果未设置，则OFF默认情况下，CMake将此属性视为。
#     能够组织到文件夹层次结构中的CMake生成器使用FOLDER目标属性来命名这些文件夹。
#     另请参阅文档FOLDER 目标属性。
#     https://cmake.org/cmake/help/v3.17/prop_gbl/USE_FOLDERS.html
#
if (WITH_SOURCE_TREE STREQUAL "hierarchical-folders")
  # 使用文件夹
  set_property(GLOBAL PROPERTY USE_FOLDERS ON)
endif()			
#
################################################################################