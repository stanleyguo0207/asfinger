################################################################################
## @brief
#			email     stanleyguo0207@163.com
#     github    https://github.com/stanleyguo0207
#     gitee     https://gitee.com/stanleyguo0207
#
################################################################################

################################################################################
## @brief
#			平台检查 位
#     检查空指针的大小 确定平台是(64-bit 还是 32-bit)
#
if(CMAKE_SIZEOF_VOID_P MATCHES 8)
  message(STATUS "Detected that the current platform is 64-bit")
  set(PLATFORM 64)
else()
  message(STATUS "Detected that the current platform is 32-bit")
  set(PLATFORM 32)
endif()
#
################################################################################

################################################################################
## @brief
#			系统平台配置
#
#     windows
if(WIN32)
  message(STATUS "Start loading Windows platform configuration")
  include("${CMAKE_SOURCE_DIR}/cmake/platform/win/settings.cmake")
#
#     unix
elseif(UNIX)
  message(STATUS "Start loading Unix platform configuration")
  include("${CMAKE_SOURCE_DIR}/cmake/platform/unix/settings.cmake")
endif()
#
################################################################################

