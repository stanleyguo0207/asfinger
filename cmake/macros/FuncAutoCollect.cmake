################################################################################
## @brief
#			email     stanleyguo0207@163.com
#			github    https://github.com/stanleyguo0207
#			gitee     https://gitee.com/stanleyguo0207
#
#  @note
#     该文件参考 TrinityCore
#
################################################################################

################################################################################
## @brief
#     Collects all source files into the given variable,
#     which is useful to include all sources in subdirectories.
#     Ignores full qualified directories listed in the variadic arguments.
#			函数
#     收集源文件，排除可变参数目录
#     @name   CollectSourceFiles
#     @param  current_dir         收集的目录
#     @param  variable            存放源文件的变量
#
#     @e.g.
#             CollectSourceFiles(
#               ${CMAKE_CURRENT_SOURCE_DIR}
#               COMMON_PRIVATE_SOURCES
#               # Exclude
#               ${CMAKE_CURRENT_SOURCE_DIR}/PrecompiledHeader
#               ${CMAKE_CURRENT_SOURCE_DIR}/Platform)
#
function(CollectSourceFiles current_dir variable)
  list(FIND ARGN "${current_dir}" IS_EXCLUDED)
  if(IS_EXCLUDED EQUAL -1)
    file(GLOB COLLECTED_SOURCES
      ${current_dir}/*.c
      ${current_dir}/*.cc
      ${current_dir}/*.cpp
      ${current_dir}/*.inl
      ${current_dir}/*.def
      ${current_dir}/*.h
      ${current_dir}/*.hh
      ${current_dir}/*.hpp)
    list(APPEND ${variable} ${COLLECTED_SOURCES})

    file(GLOB SUB_DIRECTORIES ${current_dir}/*)
    foreach(SUB_DIRECTORY ${SUB_DIRECTORIES})
      if (IS_DIRECTORY ${SUB_DIRECTORY})
        CollectSourceFiles("${SUB_DIRECTORY}" "${variable}" "${ARGN}")
      endif()
    endforeach()
    set(${variable} ${${variable}} PARENT_SCOPE)
  endif()
endfunction()
#
################################################################################

################################################################################
## @brief
#			Collects all subdirectoroies into the given variable,
#     which is useful to include all subdirectories.
#     Ignores full qualified directories listed in the variadic arguments.
#			函数
#     收集子目录，排除可变参数目录
#     @name   CollectIncludeDirectories
#     @param  current_dir         收集的目录
#     @param  variable            存放子目录集合的变量
#
#     @e.g.
#             CollectIncludeDirectories(
#               ${CMAKE_CURRENT_SOURCE_DIR}
#               COMMON_PUBLIC_INCLUDES
#               # Exclude
#               ${CMAKE_CURRENT_SOURCE_DIR}/PrecompiledHeaders
#               ${CMAKE_CURRENT_SOURCE_DIR}/Platform)
#
function(CollectIncludeDirectories current_dir variable)
  list(FIND ARGN "${current_dir}" IS_EXCLUDED)
  if(IS_EXCLUDED EQUAL -1)
    list(APPEND ${variable} ${current_dir})
    file(GLOB SUB_DIRECTORIES ${current_dir}/*)
    foreach(SUB_DIRECTORY ${SUB_DIRECTORIES})
      if (IS_DIRECTORY ${SUB_DIRECTORY})
        CollectIncludeDirectories("${SUB_DIRECTORY}" "${variable}" "${ARGN}")
      endif()
    endforeach()
    set(${variable} ${${variable}} PARENT_SCOPE)
  endif()
endfunction()
#
################################################################################