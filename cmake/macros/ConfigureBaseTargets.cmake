################################################################################
## @brief
#			email     stanleyguo0207@163.com
#			github    https://github.com/stanleyguo0207
#			gitee     https://gitee.com/stanleyguo0207
#
#  @note
#     该文件参考 TrinityCore
#
################################################################################

################################################################################
## @brief
#			编译选项接口库
#     library   asfinger-compile-option-interface
#
add_library(asfinger-compile-option-interface INTERFACE)
#
################################################################################

################################################################################
## @brief
#			关闭某些编译的额外支持 使用-std=c++11 形式
#
#     Use -std=c++11 instead of -std=gnu++11
set(CXX_EXTENSIONS OFF)
################################################################################

################################################################################
## @brief
#			特性接口库
#     library   asfinger-feature-interface
#			
add_library(asfinger-feature-interface INTERFACE)
target_compile_features(asfinger-feature-interface
  INTERFACE
    cxx_alias_templates
    cxx_auto_type
    cxx_constexpr
    cxx_decltype
    cxx_decltype_auto
    cxx_final
    cxx_lambdas
    cxx_generic_lambdas
    cxx_variadic_templates
    cxx_defaulted_functions
    cxx_nullptr
    cxx_trailing_return_types
    cxx_return_type_deduction
    cxx_override)
#
################################################################################

################################################################################
## @brief
#			通用接口库
#     library   asfinger-default-interface
#
add_library(asfinger-default-interface INTERFACE)
target_link_libraries(asfinger-default-interface
  INTERFACE
    asfinger-compile-option-interface
    asfinger-feature-interface)
#
################################################################################

################################################################################
## @brief
#			警告接口库
#     library   asfinger-warning-interface
#     这个接口通过平台配置实现			
#
add_library(asfinger-warning-interface INTERFACE)
#
################################################################################

################################################################################
## @brief
#			静默警告接口库
#     library   asfinger-no-warning-interface
#			
add_library(asfinger-no-warning-interface INTERFACE)
if(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  target_compile_options(asfinger-no-warning-interface
    INTERFACE
      /W0)
else()
  target_compile_options(asfinger-no-warning-interface
    INTERFACE
      -w)
endif()
#
################################################################################

################################################################################
## @brief
#			隐藏符号接口库
#     library   asfinger-hidden-symbols-interface
#			
add_library(asfinger-hidden-symbols-interface INTERFACE)
#
################################################################################

################################################################################
## @brief
#			依赖接口库 接口合并
#     library   asfinger-dependency-interface
#
add_library(asfinger-dependency-interface INTERFACE)
target_link_libraries(asfinger-dependency-interface
  INTERFACE
    asfinger-default-interface
    asfinger-no-warning-interface
    asfinger-hidden-symbols-interface)
#
################################################################################

################################################################################
## @brief
#			核心接口库 接口合并
#     library   asfinger-dependency-interface
#
add_library(asfinger-core-interface INTERFACE)
target_link_libraries(asfinger-core-interface
  INTERFACE
    asfinger-default-interface
    asfinger-warning-interface)
#
################################################################################