################################################################################
## @brief
#			email     stanleyguo0207@163.com
#			github    https://github.com/stanleyguo0207
#			gitee     https://gitee.com/stanleyguo0207
#
#  @note
#     该文件参考 TrinityCore
#
################################################################################

################################################################################
## @brief
#			预编译内存分配限制
#
if(MSVC)
  # Specify the maximum PreCompiled Header memory allocation limit
  # Fixes a compiler-problem when using PCH - the /Ym flag is adjusted by the compiler in MSVC2012,
  # hence we need to set an upper limit with /Zm to avoid discrepancies)
  # (And yes, this is a verified, unresolved bug with MSVC... *sigh*)
  #
  # Note: This workaround was verified to be required on MSVC 2017 as well
  set(COTIRE_PCH_MEMORY_SCALING_FACTOR 500)
endif()
#
################################################################################

################################################################################
## @brief
#			cotire 引用
#
include(cotire)
#
################################################################################

################################################################################
## @brief
#			函数
#     添加c++ pch文件
#     @name   add_cxx_pch
#     @param  TARGET_NAME_LIST    目标列表
#     @param  PCH_HEADER          pch文件
#
function(ADD_CXX_PCH TARGET_NAME_LIST PCH_HEADER)
  # 遍历所有目标
  foreach(TARGET_NAME ${TARGET_NAME_LIST})
    # 禁止统一构建
    set_target_properties(${TARGET_NAME} PROPERTIES COTIRE_ADD_UNITY_BUILD OFF)
    # 设置前缀
    set_target_properties(${TARGET_NAME} PROPERTIES COTIRE_CXX_PREFIX_HEADER_INIT ${PCH_HEADER})
    # Workaround for cotire bug: https://github.com/sakra/cotire/issues/138
    set_property(TARGET ${TARGET_NAME} PROPERTY CXX_STANDARD ${CMAKE_CXX_STANDARD})
  endforeach()

  cotire(${TARGET_NAME_LIST})
endfunction(ADD_CXX_PCH)
#
################################################################################