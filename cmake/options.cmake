################################################################################
## @brief
#			email     stanleyguo0207@163.com
#     github    https://github.com/stanleyguo0207
#     gitee     https://gitee.com/stanleyguo0207
#
################################################################################

################################################################################
## @brief
#			设置cmake兼容方案
#
if(${CMAKE_VERSION} VERSION_LESS 3.12)
  cmake_policy(VERSION            ${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION})
else()
  cmake_policy(VERSION                                                     3.12)
endif()
#
################################################################################

################################################################################
## @brief
#			发布版本
#
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE                                         "RelWithDebInfo")
endif()
#
################################################################################

################################################################################
## @brief
#			设置run path的处理，不允许cmake自动添加rpath
#
# if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
set(CMAKE_INSTALL_PREFIX        "${CMAKE_SOURCE_DIR}/stage/${CMAKE_BUILD_TYPE}")
# endif()
set(CMAKE_SKIP_BUILD_RPATH                                                    0)
set(CMAKE_BUILD_WITH_INSTALL_RPATH                                            0)
set(CMAKE_INSTALL_RPATH                           "${CMAKE_INSTALL_PREFIX}/lib")
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH                                         1)
#
################################################################################

################################################################################
## @brief
#			compile_commands.json
#
set(CMAKE_EXPORT_COMPILE_COMMANDS                                            ON)
#
################################################################################

################################################################################
## @brief
#			项目控制
#
option(SERVERS  "Build servers"                                              ON)
option(TOOLS    "Build tools"                                               OFF)
#
################################################################################

################################################################################
## @brief
#			PCH开关
#
if(NOPCH)
  option(USE_COREPCH    "Use precompiled headers when compiling servers"    OFF)
  option(USE_SCRIPTPCH  "Use precompiled headers when compiling scripts"    OFF)
else()
  option(USE_COREPCH    "Use precompiled headers when compiling servers"     ON)
  option(USE_SCRIPTPCH  "Use precompiled headers when compiling scripts"     ON)
endif()
#
################################################################################

################################################################################
## @brief
#			动态库开关
#
option(WITCH_DYNAMIC_LINKING  "Enable dynamic library linking"              OFF)
#
IsDynamicLinkingRequired(WITH_DYNAMIC_LINKING_FORCED)
#
if(WITCH_DYNAMIC_LINKING AND WITH_DYNAMIC_LINKING_FORCED)
  set(WITH_DYNAMIC_LINKING_FORCED                                           OFF)
endif()
#
if(WITCH_DYNAMIC_LINKING OR WITH_DYNAMIC_LINKING_FORCED)
  set(BUILD_SHARED_LIBS                                                      ON)
else()
  set(BUILD_SHARED_LIBS                                                     OFF)
endif()
#
if(BUILD_SHARED_LIBS)
  add_definitions(-DAFCORE_API_USE_DYNAMIC_LINKING)
endif()
#
################################################################################

################################################################################
## @brief
#			测试打开标志
#
option(BUILD_TESTS  "Build tests"                                            ON)
#
################################################################################

################################################################################
## @brief
#		  警告伴随标志
#
option(WITH_WARNINGS  "Show all warnings during compile"                    OFF)
#
################################################################################

################################################################################
## @brief
#			核心调试标志
#
option(WITH_COREDEBUG "Include additional debug-code in core"                ON)
if(WITH_COREDEBUG)
  add_definitions(-DAFCORE_COREDEBUG)
endif()
#
################################################################################

################################################################################
## @brief
#			源码树
#     @see FuncGroupSources.cmake
#     no                          不分
#     flat                        平面
#     hierarchical (default)      阶级式
#     hierarchical-folders        阶级式-文件夹
#
set(WITH_SOURCE_TREE                                              "hierarchical"
  CACHE STRING "Build the source tree for IDE's")
set_property(CACHE WITH_SOURCE_TREE PROPERTY STRINGS no flat hierarchical hierarchical-folders)
#
################################################################################