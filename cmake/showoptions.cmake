################################################################################
## @brief
#			email     stanleyguo0207@163.com
#     github    https://github.com/stanleyguo0207
#     gitee     https://gitee.com/stanleyguo0207
#
################################################################################

message("* All options info.")
message("* ------------------------------------------------------------------ *")

################################################################################
## @brief
#			c++标准
#
message("")
message("* CMAKE_CXX_STANDARD                                 : ${CMAKE_CXX_STANDARD}")
message("* CMAKE_CXX_STANDARD_REQUIRED                        : ${CMAKE_CXX_STANDARD_REQUIRED}")
message("* CMAKE_CXX_EXTENSIONS                               : ${CMAKE_CXX_EXTENSIONS}")
message("")
#
################################################################################

################################################################################
## @brief
#			发布版本
#
message("")
message("* CMAKE_BUILD_TYPE                                   : ${CMAKE_BUILD_TYPE}")
message("")
#
################################################################################

################################################################################
## @brief
#			源码与编译目录
#
message("")
message("* CMAKE_SOURCE_DIR                                   : ${CMAKE_SOURCE_DIR}")
message("* CMAKE_BINARY_DIR                                   : ${CMAKE_BINARY_DIR}")
message("* CMAKE_INSTALL_RPATH                                : ${CMAKE_INSTALL_RPATH}")
message("")
#
################################################################################

################################################################################
## @brief
#			rpath设置
#
message("")
if(CMAKE_SKIP_BUILD_RPATH)
  message("* CMAKE_SKIP_BUILD_RPATH                             : ON")
else()
  message("* CMAKE_SKIP_BUILD_RPATH                             : OFF")
endif()
if(CMAKE_BUILD_WITH_INSTALL_RPATH)
  message("* CMAKE_BUILD_WITH_INSTALL_RPATH                     : ON")
else()
  message("* CMAKE_BUILD_WITH_INSTALL_RPATH                     : OFF")
endif()
if(CMAKE_INSTALL_RPATH_USE_LINK_PATH)
  message("* CMAKE_INSTALL_RPATH_USE_LINK_PATH                  : ON")
else()
  message("* CMAKE_INSTALL_RPATH_USE_LINK_PATH                  : OFF")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			compile_commands.json
#
message("")
if(CMAKE_EXPORT_COMPILE_COMMANDS)
  message("* CMAKE_EXPORT_COMPILE_COMMANDS                      : ON (default)")
else()
  message("* CMAKE_EXPORT_COMPILE_COMMANDS                      : OFF")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			项目控制
#
message("")
if(SERVERS)
  message("* Build servers                                      : ON (default)")
else()
  message("* Build servers                                      : OFF")
endif()
if(TOOLS)
  message("* Build tools                                        : ON")
else()
  message("* Build tools                                        : OFF (default)")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			PCH开关
#
message("")
if(USE_COREPCH)
  message("* Use precompiled headers when compiling servers     : ON (default)")
else()
  message("* Use precompiled headers when compiling servers     : OFF")
endif()
if(USE_SCRIPTPCH)
  message("* Use precompiled headers when compiling scripts     : ON (default)")
else()
  message("* Use precompiled headers when compiling scripts     : OFF")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			动态库标志
#
message("")
if(BUILD_SHARED_LIBS)
  message("* Will link against shared libraries!                : ON")
  if(WITH_DYNAMIC_LINKING_FORCED)
    message("* Dynamic linking was enforced through a dynamic script module!")
  endif()
  WarnAboutSpacesInBuildPath()
else()
  message("* Will link against shared libraries!                : OFF (default)")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			测试打开标志
#
message("")
if(BUILD_TESTS)
  message("* Build tests                                        : ON")
else()
  message("* Build tests                                        : OFF (default)")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			警告伴随标志
#
message("")
if(WITH_WARNINGS)
  message("* Show all warnings during compile                   : ON")
else()
  message("* Show all warnings during compile                   : OFF (default)")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			核心调试标志
#
message("")
if(WITH_COREDEBUG)
  message("* Include additional debug-code in core              : ON")
else()
  message("* Include additional debug-code in core              : OFF (default)")
endif()
message("")
#
################################################################################

################################################################################
## @brief
#			源码树选项
#
message("")
if(NOT ${WITH_SOURCE_TREE} STREQUAL "")
  message("* Build the source tree for IDE's.                   : ${WITH_SOURCE_TREE}")
else()
  message("* Build the source tree for IDE's.                   : OFF (default)")
endif()
message("")
#
################################################################################

message("* ------------------------------------------------------------------ *")
