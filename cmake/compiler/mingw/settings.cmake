################################################################################
## @brief
#			email     stanleyguo0207@163.com
#     github    https://github.com/stanleyguo0207
#     gitee     https://gitee.com/stanleyguo0207
#
################################################################################

##########h######################################################################
## @brief
#			设置可执行二进制的输出路径
#     在支持DLL的平台上为.exe文件和.dll文件
#
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/bin")
#
################################################################################

################################################################################
## @brief
#			编译选项 构建类型
#
target_compile_definitions(asfinger-compile-option-interface
  INTERFACE
    -D_BUILD_DIRECTIVE="${CMAKE_BUILD_TYPE}")
#
################################################################################

################################################################################
## @brief
#			32位平台 自动矢量化
#
if(PLATFORM EQUAL 32)
  target_compile_options(asfinger-compile-option-interface
    INTERFACE
      -msse2
      -mfpmath=sse)
endif()
#
target_compile_definitions(asfinger-compile-option-interface
  INTERFACE
    -DHAVE_SSE2
    -D__SSE2__)
message(STATUS "GCC: SFMT enabled, SSE2 flags forced")
#
################################################################################

################################################################################
## @brief
#			警告编译接口
#
if(WITH_WARNINGS)
  target_compile_options(asfinger-warning-interface
    INTERFACE
      -W
      -Wall
      -Wextra
      -Winit-self
      -Winvalid-pch
      -Wfatal-errors
      -Woverloaded-virtual)
  message(STATUS "GCC: All warnings enabled")
endif()
#
################################################################################

################################################################################
## @brief
#			核心调试编译选项接口
#
if(WITH_COREDEBUG)
  target_compile_options(asfinger-compile-option-interface
    INTERFACE
      -g3)

  message(STATUS "GCC: Debug-flags set (-g3)")
endif()
#
################################################################################