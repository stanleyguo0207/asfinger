```
                               █████╗ ███████╗███████╗██╗███╗   ██╗ ██████╗ ███████╗██████╗ 
                              ██╔══██╗██╔════╝██╔════╝██║████╗  ██║██╔════╝ ██╔════╝██╔══██╗
                              ███████║███████╗█████╗  ██║██╔██╗ ██║██║  ███╗█████╗  ██████╔╝
                              ██╔══██║╚════██║██╔══╝  ██║██║╚██╗██║██║   ██║██╔══╝  ██╔══██╗
                              ██║  ██║███████║██║     ██║██║ ╚████║╚██████╔╝███████╗██║  ██║
                              ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝
```

- [c++新特性](#c%e6%96%b0%e7%89%b9%e6%80%a7)
  - [`enable_if`](#enableif)
  - [`string_view`](#stringview)
  - [`[[fallthrough]]`](#fallthrough)

# c++新特性

## `enable_if`

`std::enable_if` `c++11`起，来自`<type_traits>`。
资料参考:https://zh.cppreference.com/w/cpp/types/enable_if

```cpp
// c++11起
template< bool B, class T = void >
struct enable_if;

// c++14起
template< bool B, class T = void >
using enable_if_t = typename enable_if<B, T>::type;
```

可能的实现:
```cpp
template<bool B, class T = void>
struct enable_if {};

template<class T>
struct enable_if<true, T> { typedef T type; };
```

看一个例子了解enable_if是干什么的。
```cpp
// demo
typename std::enable_if<true, int>::type t; //正确
typename std::enable_if<true>::type; //可以通过编译，没有实际用处，推导的模板是偏特化版本，第一模板参数是true，第二模板参数是通常版本中定义的默认类型即void
typename std::enable_if<false>::type; //无法通过编译，type类型没有定义
typename std::enable_if<false, int>::type t2; //同上
```

当B特化为true的时候，后面的类型才会启用typedef。

## `string_view`

`std::string_view` c++17起，来自`<string_view>`。
参考资料:https://zh.cppreference.com/w/cpp/string/basic_string_view

`string_view`用来解决字符串的拷贝开销。

```cpp
#include <cstdio>
#include <cinttypes>

#include <string>
#include <string_view>

void* operator new(std::size_t count) {
    printf("\n%s:%i in %s 分配了堆内存 %" PRIu64 "字节\n", __FILE__, __LINE__, __FUNCTION__, count);
    return malloc(count);
}

void operator delete(void* p) {
    printf("%s:%i in %s 释放堆内存 %p\n", __FILE__, __LINE__, __FUNCTION__, p);
    free(p);
}

void PrintStr(const std::string& str) {
    std::string tmp = str;
    printf("%s:%i in %s str内存地址 %p\n", __FILE__, __LINE__, __FUNCTION__, str.data());
    printf("%s:%i in %s tmp内存地址 %p\n", __FILE__, __LINE__, __FUNCTION__, tmp.data());
}

void PrintStv(std::string_view stv) {
    printf("%s:%i in %s str内存地址 %p\n", __FILE__, __LINE__, __FUNCTION__, stv.data());
}

int main(void) {
    printf("-------初始化string str对象\n");
    std::string str = "How do you do~,My name is peter!";
    printf("str 的地址为 %p\n", str.data());

    printf("-------初始化string_view stv对象\n");
    std::string_view stv(str.c_str(), str.size());

    printf("-------PrintStr\n");
    PrintStr(str);

    printf("-------PrintStr 字符串字面量直接传参方式\n");
    PrintStr("How do you do~,My name is peter!");

    printf("-------PrintStv\n");
    PrintStv(stv);

  printf("-------PrintStv 字符串字面量直接传参方式\n");
    PrintStv("How do you do~,My name is peter!");

    return 0;
}
```

输出如下:
```markdown
> -------初始化string str对象

> ...\main.cpp:8 in operator new 分配了堆内存 48字节
> str 的地址为 000002072D2EA420
> -------初始化string_view stv对象
> -------PrintStr

> ...\main.cpp:8 in operator new 分配了堆内存 48字节
> ...\main.cpp:19 in PrintStr str内存地址 000002072D2EA420
> ...\main.cpp:20 in PrintStr tmp内存地址 000002072D2EAEE0
> ...\main.cpp:13 in operator delete 释放堆内存 000002072D2EAEE0
> -------PrintStr 字符串字面量直接传参方式

> ...\main.cpp:8 in operator new 分配了堆内存 48字节

> ...\main.cpp:8 in operator new 分配了堆内存 48字节
> ...\main.cpp:19 in PrintStr str内存地址 000002072D2EAA20
> ...\main.cpp:20 in PrintStr tmp内存地址 000002072D2EAC20
> ...\main.cpp:13 in operator delete 释放堆内存 000002072D2EAC20
> ...\main.cpp:13 in operator delete 释放堆内存 000002072D2EAA20
> -------PrintStv
> ...\main.cpp:24 in PrintStv str内存地址 000002072D2EA420
> -------PrintStv 字符串字面量直接传参方式
> ...\main.cpp:24 in PrintStv str内存地址 00007FF6903533D0
> ...\main.cpp:13 in operator delete 释放堆内存 000002072D2EA420
```

## `[[fallthrough]]`

`[[fallthrough]]` c++17起，来自语言属性。
参考资料:https://zh.cppreference.com/w/cpp/language/attributes/fallthrough

指示从前一标号直落是有意的，而在发生直落时给出警告的编译器不应诊断它。

一个来自官方的demo:
```cpp
void f(int n) {
  void g(), h(), i();
  switch (n) {
    case 1:
    case 2:
      g();
     [[fallthrough]];
    case 3: // 直落时不警告
      h();
    case 4: // 编译器可在发生直落时警告
      if(n < 3) {
          i();
          [[fallthrough]]; // OK
      }
      else {
          return;
      }
    case 5:
      while (false) {
        [[fallthrough]]; // 非良构：下一语句不是同一迭代的一部分
      }
    case 6:
      [[fallthrough]]; // 非良构：无后继的 case 或 default 标号
  }
}
```