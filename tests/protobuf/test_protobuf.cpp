#include "../include.h"

#include <iostream>

#include <google/protobuf/stubs/common.h>

TEST_CASE("Test protobuf", "[protobuf]") {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  std::shared_ptr<void> protobuf_handle(nullptr,
    [](void*) {
      google::protobuf::ShutdownProtobufLibrary();
    });

  std::cout << "protobuf test" << std::endl;
}