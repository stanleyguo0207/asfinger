#include "../include.h"
/// c++17 mingw有问题 暂时不支持
// #include <filesystem>

#include <boost/filesystem/operations.hpp>

#include "config.h"

// namespace fs = std::filesystem;
namespace fs = boost::filesystem;

#ifndef _ASFINGER_TEST_CONFIG
  #define _ASFINGER_TEST_CONFIG "test.conf"
#endif

TEST_CASE("Test config", "[config]") {
  using namespace afcore;

  auto config_file = fs::absolute(_ASFINGER_TEST_CONFIG);

  auto& cfgmgr = CConfigMgr::Instance();
  std::string config_error;
   if (!cfgmgr.Load(config_file.generic_string(), {}, config_error)) {
     printf("Error in config file:%s\n", config_error.c_str());
   }

   printf("filename = %s\n", cfgmgr.GetFilename().c_str());
   printf("Name = %s\n", cfgmgr.GetStringDefault("Name", "not found").c_str());
   printf("Test_Int = %d\n", cfgmgr.GetIntDefault("Test_Int", 0));
   printf("Test_Float = %f\n", cfgmgr.GetFloatDefault("Test_Float", 0.0));
//  REQUIRE(cfgmgr->Load(config_file.generic_string(), {}, config_error));
//  REQUIRE(config_error.empty());
//  REQUIRE(!cfgmgr->GetStringDefault("Name", "not found").compare("test"));
//  REQUIRE(6 == cfgmgr->GetIntDefault("Test_Int", 0));
//  REQUIRE(3.14f == cfgmgr->GetFloatDefault("Test_Float", 0.0));
}