#include "../include.h"

#include "report.h"

TEST_CASE("Test DBG_ASSERT", "[debug]") {
  using namespace afcore;

  // int n = 200;
  // DBG_ASSERT(n, "assert false %d", n);
  // n = 0;
  // DBG_ASSERT(n, "assert false %d", n);
}

TEST_CASE("Test DBG_FATAL", "[debug]") {
  using namespace afcore;

  // int n = 200;
  // DBG_FATAL(n, "fatal false %d", n);
  // n = 0;
  // DBG_FATAL(n, "fatal false %d", n);
}

TEST_CASE("Test DBG_WARNING", "[debug]") {
  using namespace afcore;

  int n = 200;
  DBG_WARNING(n, "warning false");
  n = 0;
  DBG_WARNING(n, "warning false");
}

TEST_CASE("Test DBG_ERROR", "[debug]") {
  using namespace afcore;

  // int n = 200;
  // DBG_ERROR(n, "error false");
  // n = 0;
  // DBG_ERROR(n, "error false");
}

TEST_CASE("Test DBG_ABORT", "[debug]") {
  using namespace afcore;

  // DBG_ABORT();
}