################################################################################
## @brief
#			email     stanleyguo0207@163.com
#			github    https://github.com/stanleyguo0207
#			gitee     https://gitee.com/stanleyguo0207
#
################################################################################

cmake_minimum_required(VERSION 3.12)

project(test_asio CXX)

add_executable(test_asio
  ../include.h
  ../main.cpp
  test_asio.cpp)
target_link_libraries(test_asio
  Catch2::Catch2
  common)

if(WIN32)
  if(MINGW)
    target_link_libraries(test_asio
      ws2_32)
  endif()
endif()

include(CTest)
include(ParseAndAddCatchTests)
ParseAndAddCatchTests(test_asio)