#include "../include.h"

#include <cstdio>

#include "database_mysql_api.h"
#include "database_env.h"

TEST_CASE("Test mysql connection", "[database mysql]") {
  using namespace afcore;
  using namespace afcore::database;

  printf("test start\n");

  mysql::LibraryInit();

  g_database_test.SetConnectionInfo("127.0.0.1;3306;root;123456;mysql", 2, 2);

  g_database_test.Open();

  auto result = g_database_test.Query("SELECT * FROM USER");

  g_database_test.Close();

  mysql::LibraryEnd();
}