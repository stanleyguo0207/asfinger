################################################################################
## @brief
#			email     stanleyguo0207@163.com
#			github    https://github.com/stanleyguo0207
#			gitee     https://gitee.com/stanleyguo0207
#
################################################################################

cmake_minimum_required(VERSION 3.12)

project(test_utility CXX)

add_executable(test_utility
  ../include.h
  ../main.cpp
  test_utils.cpp
  test_rand.cpp)
target_link_libraries(test_utility
  Catch2::Catch2
  common)
include(CTest)
include(ParseAndAddCatchTests)
ParseAndAddCatchTests(test_utility)