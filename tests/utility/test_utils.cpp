#include "../include.h"

#include <cstring>

#include <string>

#include "utils.h"

TEST_CASE("Test StringToBool", "[utils]") {
  using namespace afcore;

  REQUIRE(true == StringToBool("1"));
  REQUIRE(true == StringToBool("true"));
  REQUIRE(true == StringToBool("yes"));
  REQUIRE(false == StringToBool("false"));
  REQUIRE(false == StringToBool("no"));
}

TEST_CASE("Test Trim", "[utils]") {
  using namespace afcore;

  std::string str1 = "  hello world  ";
  std::string str2 = "  hello world  ";
  std::string str3 = "  hello world  ";

  TrimLeft(str1);
  TrimRight(str2);
  Trim(str3);

  REQUIRE(0 == str1.compare("hello world  "));
  REQUIRE(0 == str2.compare("  hello world"));
  REQUIRE(0 == str3.compare("hello world"));
}

TEST_CASE("Test CTokenizer", "[utils]") {
  using namespace afcore;

  std::string str = "gameserver;127.0.0.1;8080";

  CTokenizer tokens(str, ';');

  REQUIRE(3 == tokens.size());
  REQUIRE(0 == strcmp("gameserver", tokens[0]));
  REQUIRE(0 == strcmp("127.0.0.1", tokens[1]));
  REQUIRE(0 == strcmp("8080", tokens[2]));

  REQUIRE(0 == strcmp("gameserver;127.0.0.1;8080", str.c_str()));
  CTokenizer tokens2(str, '.');
  REQUIRE(4 == tokens2.size());
}