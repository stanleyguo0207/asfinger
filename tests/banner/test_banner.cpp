#include "../include.h"

#include "log.h"
#include "banner.h"
#include "appender_console.h"
TEST_CASE("Test banner", "[banner]") {
  using namespace afcore;

  auto logger = afcore::Create<RAppenderConsoleStdout_MT>("banner");

  banner::Show("asfinger",
    [&logger](const char* text) {
      logger->Info(text);
    }, nullptr);
}


TEST_CASE("Test banner2", "[banner2]") {
  using namespace afcore;

  auto logger = afcore::Create<RAppenderConsoleStdout_MT>("banner");

  banner::Show2("asfinger",
                [&logger](const char* text) {
                  logger->Info(text);
                }, nullptr);
}