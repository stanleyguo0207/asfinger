```
                               █████╗ ███████╗███████╗██╗███╗   ██╗ ██████╗ ███████╗██████╗ 
                              ██╔══██╗██╔════╝██╔════╝██║████╗  ██║██╔════╝ ██╔════╝██╔══██╗
                              ███████║███████╗█████╗  ██║██╔██╗ ██║██║  ███╗█████╗  ██████╔╝
                              ██╔══██║╚════██║██╔══╝  ██║██║╚██╗██║██║   ██║██╔══╝  ██╔══██╗
                              ██║  ██║███████║██║     ██║██║ ╚████║╚██████╔╝███████╗██║  ██║
                              ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝
```
<a id="top">![Top][pTop]</a>
- [asfinger](#asfinger)
  - [介绍](#%e4%bb%8b%e7%bb%8d)
  - [随笔](#%e9%9a%8f%e7%ac%94)

# asfinger

## 介绍
游戏服务器框架

asfinger是基于c++17的高性能的游戏服务器框架。

本来是决定只针对linux的框架，为了挑战自己，准备写个跨平台的。

<a href="#top">![Touch][pTouch]</a>

## 随笔

![Touch][pView][点点滴滴的记录][urlSomenotes]

<a href="#top">![Touch][pTouch]</a>

<!-- image -->
[pTouch]:       ./assets/image/readme/touch.png
[pTop]:         ./assets/image/readme/top.png
[pView]:        ./assets/image/readme/view.png

<!-- url -->
[urlSomenotes]: ./doc/somenotes.md