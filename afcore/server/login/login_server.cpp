/// @brief  登录服务器


#include <csignal>

#include <iostream>

#include <boost/filesystem//operations.hpp>

#include "log.h"
#include "logcommon.h"
#include "logger.h"
#include "report.h"
#include "config.h"
#include "banner.h"
#include "appender_file_daily.h"
#include "appender_console_color.h"

#ifndef __AFCORE_LOGIN_CONFIG
# define __AFCORE_LOGIN_CONFIG "login.conf"
#endif

namespace fs = boost::filesystem;

int main(int argc, char** argv) {
  signal(SIGABRT, &afcore::RptAbortHandler);

  auto config_file = fs::absolute(__AFCORE_LOGIN_CONFIG);
  std::string config_error;
  if (!afcore::CConfigMgr::Instance().Load(
    config_file.generic_string(),
    {},
    config_error)) {
    printf("Error in config file %s\n", config_error.c_str());
    return 1;
  }

  // 每日日志
  auto daily_file_appender = std::make_shared<afcore::RAppenderFileDaily_MT>(
    afcore::CConfigMgr::Instance().GetStringDefault("Daily_Base_File_Path", "logs/") +
    afcore::CConfigMgr::Instance().GetStringDefault("Daily_Base_File_Name", "daily.txt"),
    afcore::CConfigMgr::Instance().GetIntDefault("Daily_Base_File_Rotation_Hour", 0),
    afcore::CConfigMgr::Instance().GetIntDefault("Daily_Base_File_Rotation_Minute", 0),
    afcore::CConfigMgr::Instance().GetBoolDefault("Daily_Base_File_Truncate", false),
    afcore::CConfigMgr::Instance().GetIntDefault("Daily_Base_File_Max", 0)
    );
  daily_file_appender->SetLevel(afcore::log::ToLevelEnum(afcore::CConfigMgr::Instance().GetStringDefault("Log_Level_Daily_File", "trace")));

  // 控制台
  auto console_appender = std::make_shared<afcore::RAppenderCorlorConsoleStdout_MT>();
  console_appender->SetLevel(afcore::log::ToLevelEnum(afcore::CConfigMgr::Instance().GetStringDefault("Log_Level_Console", "trace")));

  afcore::CLogger default_logger("default", {console_appender, daily_file_appender});
  auto default_logger_sptr = std::make_shared<afcore::CLogger>(default_logger);
  default_logger_sptr->SetLogLevel(afcore::log::ToLevelEnum(afcore::CConfigMgr::Instance().GetStringDefault("Default_Logger_Log_Level", "trace")));
  default_logger_sptr->SetFlushLevel(afcore::log::ToLevelEnum(afcore::CConfigMgr::Instance().GetStringDefault("Default_Logger_Flush_Level", "trace")));

  afcore::InitializeLogger(default_logger_sptr);
  afcore::SetDefaultLogger(default_logger_sptr);
  afcore::SetAutomaticRegistration(true);

  afcore::SetPattern("%v");
  // banner
  afcore::banner::Show(
    "loginserver",
    [](const char* text) {
      LOG_INFO(text);
    },
    []() {

    });
  afcore::SetPattern("%+");

  LOG_INFO("login server start");

  return 0;
}