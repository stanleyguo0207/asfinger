#ifndef AFCORE_COMMON_NOCOPYABLE_
#define AFCORE_COMMON_NOCOPYABLE_

namespace afcore {

class CNocopyable {
public:
  CNocopyable(const CNocopyable&) = delete;
  CNocopyable operator=(const CNocopyable&) = delete;
protected:
  CNocopyable() = default;
  ~CNocopyable() = default;

  CNocopyable(CNocopyable&&) = default;
  CNocopyable& operator=(CNocopyable&&) = default;
};

} // !namespace afcore

#endif // !AFCORE_COMMON_NOCOPYABLE_