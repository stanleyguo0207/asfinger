#ifndef AFCORE_COMMON_LOG_HELPER_HELPER_
#define AFCORE_COMMON_LOG_HELPER_HELPER_

#include <mutex>

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  假互斥锁
struct SNullMutex {
  /// @note   std::mutex 接口
  void lock() const {}
  void unlock() const {}
  [[nodiscard]] bool try_lock() const {
    return true;
  }
};

/// @brief  控制台互斥锁
struct SConsoleMutex {
  using RMutex = std::mutex;
  /// @brief 获取互斥锁
  /// @return 互斥锁
  static RMutex& mutex() {
    static RMutex s_mutex;
    return s_mutex;
  }
};

/// @brief  控制台假互斥锁
struct SConsoleNullMutex {
  using RMutex = SNullMutex;
  /// @brief 获取互斥锁
  /// @return 假互斥锁
  static RMutex& mutex() {
    static RMutex s_mutex;
    return s_mutex;
  }
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_HELPER_HELPER_