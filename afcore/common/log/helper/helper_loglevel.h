#ifndef AFCORE_COMMON_LOG_HELPER_HELPER_LOGLEVEL_
#define AFCORE_COMMON_LOG_HELPER_HELPER_LOGLEVEL_

#include <string>
#include <unordered_map>

#include "logcommon.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  日志级别辅助
class CHelperLogLevel {
public:
  /// @brief  设置日志级别
  /// @param  logger_name     记录器名称
  /// @param  l               日志级别
  void SetLevelByLoggerName(const std::string& logger_name, ELogLevel l) {
    if (logger_name.empty()) {
      default_level_ = l;
    } else {
      levels_[logger_name] = l;
    }
  }

  /// @brief  设置默认日志级别
  /// @param  l               日志级别
  void SetDefaultLevel(ELogLevel l) {
    default_level_ = l;
  }

  /// @brief  获取日志级别
  /// @param  logger_name     记录器名称
  /// @return 对应记录器的日志级别
  ELogLevel GetLevelByLoggerName(const std::string& logger_name) {
    auto iter = levels_.find(logger_name);
    return levels_.end() != iter ? iter->second : default_level_;
  }

  /// @brief  获取默认日志级别
  /// @return 默认日志级别
  ELogLevel GetDefaultLevel() {
    return default_level_;
  }
private:
  std::unordered_map<std::string, ELogLevel> levels_; ///< 所有记录器日志级别信息
  ELogLevel default_level_ {kLogLevelInfo};           ///< 默认日志级别
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_HELPER_HELPER_LOGLEVEL_