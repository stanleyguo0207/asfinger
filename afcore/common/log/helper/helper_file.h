#ifndef AFCORE_COMMON_LOG_HELPER_HELPER_FILE_
#define AFCORE_COMMON_LOG_HELPER_HELPER_FILE_

#include <tuple>

#include "logcommon.h"
#include "nocopyable.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  文件帮助类
class AFCORE_COMMON_API CHelperFile
  : public CNocopyable {
public:
  /// @brief  默认构造函数
  explicit CHelperFile() = default;
  /// @brief  熊沟函数
  ~CHelperFile();
  /// @brief  打开文件
  /// @param  filename    文件名
  /// @param  truncate    截断
  void Open(const RFileName& filename, bool truncate = false);
  /// @brief  重新打开文件
  /// @param  truncate    截断
  void Reopen(bool truncate);
  /// @brief  刷新
  void Flush();
  /// @brief  关闭
  void Close();
  /// @brief  写文件
  /// @param  buf         fmt缓冲
  void Write(const RMemoryBuf& buf);
  /// @brief  文件大小
  /// @return 文件大小
  size_t Size() const;
  /// @brief  文件名
  /// @return 文件名
  const RFileName& FileName() const;
  /// @brief
  static std::tuple<RFileName, RFileName> SplitByExtension(const RFileName& full_name);

private:
  const int open_tries_ {5};      ///< 打开尝试次数
  const int open_interval_ {10};  ///< 打开间隔 毫秒
  std::FILE* fd_ {nullptr};       ///< 文件句柄
  RFileName filename_;            ///< 文件名
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_HELPER_HELPER_FILE_