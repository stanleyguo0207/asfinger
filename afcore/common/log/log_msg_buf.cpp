#include "log_msg_buf.h"

namespace afcore {
namespace log {

CLogMessageBuffer::CLogMessageBuffer(const SLogMessage& log_msg)
  : SLogMessage{log_msg} {
  buffer_.append(logger_name.begin(), logger_name.end());
  buffer_.append(playload.begin(), playload.end());
  UpdateBuffer();
}

CLogMessageBuffer::CLogMessageBuffer(const CLogMessageBuffer& that)
  : SLogMessage{that} {
  buffer_.append(logger_name.begin(), logger_name.end());
  buffer_.append(playload.begin(), playload.end());
  UpdateBuffer();
}

CLogMessageBuffer::CLogMessageBuffer(CLogMessageBuffer&& that) noexcept
  : SLogMessage{that}
  , buffer_{std::move(that.buffer_)} {
  UpdateBuffer();
}

CLogMessageBuffer& CLogMessageBuffer::operator=(const CLogMessageBuffer& that) {
  if (this != &that) {
    SLogMessage::operator=(that);
    buffer_.clear();
    buffer_.append(that.buffer_.data(), that.buffer_.data() + that.buffer_.size());
    UpdateBuffer();
  }
  return *this;
}

CLogMessageBuffer& CLogMessageBuffer::operator=(CLogMessageBuffer&& that) noexcept {
  if (this != &that) {
    SLogMessage::operator=(that);
    buffer_ = std::move(that.buffer_);
    UpdateBuffer();
  }
  return *this;
}

void CLogMessageBuffer::UpdateBuffer() {
  logger_name = RStringView{buffer_.data(), logger_name.size()};
  playload = RStringView{buffer_.data() + logger_name.size(), playload.size()};
}

} // !namespace log
} // !namespace afcore