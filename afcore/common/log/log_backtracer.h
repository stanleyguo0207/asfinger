#ifndef AFCORE_COMMON_LOG_LOG_BACKTRACER_
#define AFCORE_COMMON_LOG_LOG_BACKTRACER_

#include <atomic>
#include <mutex>
#include <functional>

#include "logcommon.h"
#include "log_msg_buf.h"
#include "circular_queue.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  日志追踪
class AFCORE_COMMON_API CLogBackTracer {
public:
  /// @brief  构造函数 默认
  CLogBackTracer() = default;
  /// @brief  拷贝构造函数
  /// @param  that    日志追踪
  CLogBackTracer(const CLogBackTracer& that);
  /// @brief  移动构造
  /// @param  that    日志追踪
  CLogBackTracer(CLogBackTracer&& that) noexcept;
  /// @brief  拷贝赋值函数
  /// @param  that    日志追踪
  /// @return 构造好的对象
  CLogBackTracer& operator=(CLogBackTracer that);

  /// @brief  设置追踪器可用并设置大小
  /// @param  size    追踪器记录最大容量
  void Enable(size_t size);

  /// @brief  设置追踪器不可用
  void Disable();

  /// @brief  获取追踪器是否可用
  /// @return true 可用<br>
  ///         false 不可用
  bool Enabled() const;

  /// @brief  追加追踪日志
  /// @param  msg     追踪日志
  void Append(const SLogMessage& msg);

  /// @brief  弹出所有追踪日志，并在fun中调用
  /// @param  fun     弹出的追踪信息处理函数
  void ForeachPop(std::function<void(const SLogMessage&)> fun);
private:
  mutable std::mutex mutex_;                      ///< 互斥锁
  std::atomic<bool> enable_ {false};           ///< 是否可用 原子
  CCircularQueue<CLogMessageBuffer> messages_;    ///< 追踪信息
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_LOG_BACKTRACER_