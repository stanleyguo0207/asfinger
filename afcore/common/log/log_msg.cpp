#include "log_msg.h"

#include "os.h"

namespace afcore {
namespace log {

SLogMessage::SLogMessage(SSrcLocInfo src_info, RStringView logger_name_in, ELogLevel l, RStringView msg)
  : logger_name(logger_name_in)
  , level(l)
  , time(RClockLog::now())
  , thread_id(GetThreadId())
  , source_info(src_info)
  , playload(msg) {

}

/// @brief  委托构造函数
SLogMessage::SLogMessage(RStringView logger_name_in, ELogLevel l, RStringView msg)
  : SLogMessage(SSrcLocInfo{}, logger_name_in, l, msg) {

}

} // !namespace log
} // !namespace afcore