#ifndef AFCORE_COMMON_LOG_PERIODIC_WORKER_
#define AFCORE_COMMON_LOG_PERIODIC_WORKER_

#include <mutex>
#include <thread>
#include <condition_variable>

#include "nocopyable.h"
#include "duration.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  周期工作线程
class AFCORE_COMMON_API CPeriodicWorker
  : public CNocopyable {
public:
  /// @brief  构造函数
  /// @param  callback        回调函数
  /// @param  interval        周期间隔
  CPeriodicWorker(const std::function<void()> &callback, RSeconds interval);
  /// @brief  析构函数
  ~CPeriodicWorker();
private:
  bool active_ {false};         ///< 可用标志
  std::thread worker_thread_;   ///< 工作线程
  std::mutex mutex_;            ///< 互斥锁
  std::condition_variable cv_;  ///< 条件变量
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_PERIODIC_WORKER_