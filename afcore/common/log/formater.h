#ifndef AFCORE_COMMON_LOG_FORMATER_
#define AFCORE_COMMON_LOG_FORMATER_

#include "logcommon.h"
#include "log_msg.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  格式器
class AFCORE_COMMON_API CFormatter {
public:
  /// @brief  析构函数 默认
  virtual ~CFormatter() = default;
  /// @brief  日志格式化 纯虚
  /// @param  msg     日志
  /// @param  [out]   dest    fmt缓冲区
  virtual void Format(const SLogMessage& msg, RMemoryBuf& dest) = 0;
  /// @brief  复制格式器 纯虚
  [[nodiscard]] virtual std::unique_ptr<CFormatter> Clone() const = 0;
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_FORMATER_
