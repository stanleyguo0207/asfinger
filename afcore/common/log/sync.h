#ifndef AFCORE_COMMON_LOG_SYNC_
#define AFCORE_COMMON_LOG_SYNC_

#include "log_hub.h"

namespace afcore {

/// @brief  日志
namespace log {

class CLogger;

/// @brief  同步记录器工厂
struct SSyncFactory {
  /// @brief  创建同步记录器
  /// @tparam Appender        同步记录器类型
  /// @tparam ...Args         记录器所需参数
  /// @param  logger_name     记录器名称
  /// @param  ...args         记录器所需参数
  /// @return 同步记录器
  template<typename Appender, typename... Args>
  static std::shared_ptr<CLogger> Create(std::string logger_name, Args&&... args) {
    auto appender = std::make_shared<Appender>(std::forward<Args>(args)...);
    auto new_logger = std::make_shared<CLogger>(std::move(logger_name), std::move(appender));
    CLogHub::Instance().InitializeLogger(new_logger);
    return new_logger;
  }
};

} // !namespace log

} // !namespace afcore

#endif //AFCORE_COMMON_LOG_SYNC_