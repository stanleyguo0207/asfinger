#ifndef AFCORE_COMMON_LOG_LOG_MSG_
#define AFCORE_COMMON_LOG_LOG_MSG_

#include "logcommon.h"
#include "duration.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  日志信息
struct AFCORE_COMMON_API SLogMessage {
  /// @brief  构造函数 默认
  SLogMessage() = default;
  /// @brief  构造函数 带源文件信息
  /// @param  src_info        源文件信息
  /// @param  logger_name_in  记录器名称
  /// @param  l               日志级别
  /// @param  msg             日志信息
  SLogMessage(SSrcLocInfo src_info, RStringView logger_name_in, ELogLevel l, RStringView msg);
  /// @brief  构造函数 不带源文件信息
  /// @param  logger_name_in  记录器名称
  /// @param  l               日志级别
  /// @param  msg             日志信息
  SLogMessage(RStringView logger_name_in, ELogLevel l, RStringView msg);
  /// @brief  拷贝构造函数 默认
  SLogMessage(const SLogMessage& that) = default;

  RStringView logger_name;                ///< 记录器名称
  ELogLevel level {kLogLevelOff};         ///< 日志级别
  RClockLog::time_point time;             ///< 时间点
  size_t thread_id {0};                   ///< 线程id
  mutable size_t color_range_start {0};   ///< 颜色包裹范围开始
  mutable size_t color_range_end {0};     ///< 颜色包裹范围结束
  SSrcLocInfo source_info;                ///< 源文件定位信息
  RStringView playload;                   ///< 有效数据
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_LOG_MSG_
