#include "log.h"

#include "formater_pattern.h"

namespace afcore {

using namespace log;

void InitializeLogger(RLoggerSptr new_logger) {
  CLogHub::Instance().InitializeLogger(std::move(new_logger));
}

RLoggerSptr GetLoggerByName(const std::string& logger_name) {
  return CLogHub::Instance().GetLoggerByName(logger_name);
}

void SetFormatter(RFormatterUptr formatter) {
  CLogHub::Instance().SetFormatter(std::move(formatter));
}

void SetPattern(std::string pattern, EPatternTimeType time_type) {
  SetFormatter(std::unique_ptr<CFormatter>(new CFormaterPattern(std::move(pattern), time_type)));
}

void EnableBacktrace(size_t msg_max) {
  CLogHub::Instance().EnableBacktrace(msg_max);
}

void DisableBacktrace() {
  CLogHub::Instance().DisableBacktrace();
}

void DumpBacktrace() {
  DefaultLoggerRaw()->DumpBacktrace();
}

void SetLogLevel(ELogLevel log_level) {
  CLogHub::Instance().SetLogLevel(log_level);
}

void SetFlushLevel(ELogLevel log_level) {
  CLogHub::Instance().SetFlushLevel(log_level);
}

void FlushEvery(RSeconds interval) {
  CLogHub::Instance().FlushEvery(interval);
}

void SetErrorHander(RErrHanlder handler) {
  CLogHub::Instance().SetErrorHander(handler);
}

void RegisterLogger(RLoggerSptr new_logger) {
  CLogHub::Instance().RegisterLogger(std::move(new_logger));
}

void ApplyAll(const std::function<void(const RLoggerSptr)> &fun) {
  CLogHub::Instance().ApplyAll(fun);
}

void Drop(const std::string &logger_name) {
  CLogHub::Instance().Drop(logger_name);
}

void DropAll() {
  CLogHub::Instance().DropAll();
}

void Shutdown() {
  CLogHub::Instance().Shutdown();
}

void SetAutomaticRegistration(bool automatic_registration) {
  CLogHub::Instance().SetAutomaticRegistration(automatic_registration);
}

RLoggerSptr DefaultLogger() {
  return CLogHub::Instance().GetDefaultLogger();
}

CLogger *DefaultLoggerRaw() {
  return CLogHub::Instance().GetDefaultRaw();
}

void SetDefaultLogger(RLoggerSptr new_default_logger) {
  CLogHub::Instance().SetDefaultLogger(std::move(new_default_logger));
}

} // !namespace afcore