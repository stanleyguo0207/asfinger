#ifndef AFCORE_COMMON_LOG_LOG_MSG_BUF_
#define AFCORE_COMMON_LOG_LOG_MSG_BUF_

#include "log_msg.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  日志信息缓冲
class AFCORE_COMMON_API CLogMessageBuffer
  : public SLogMessage {
public:
  /// @brief  构造函数 默认
  CLogMessageBuffer() = default;
  /// @brief  构造函数 显示 使用父类构造
  explicit CLogMessageBuffer(const SLogMessage& log_msg);
  /// @brief  拷贝构造函数
  /// @param  that    日志信息缓冲
  CLogMessageBuffer(const CLogMessageBuffer& that);
  /// @brief  移动构造
  /// @param  [out]   that    日志信息缓冲
  CLogMessageBuffer(CLogMessageBuffer&& that) noexcept;
  /// @brief  拷贝赋值函数
  /// @param  that    日志信息缓冲
  /// @return 构造好的对象
  CLogMessageBuffer& operator=(const CLogMessageBuffer& that);
  /// @brief  移动赋值函数
  /// @param  [out]   that    日志信息缓冲
  /// @return 构造好的对象
  CLogMessageBuffer& operator=(CLogMessageBuffer&& that) noexcept;
private:
  /// @brief  更新缓冲区
  void UpdateBuffer();
private:
  RMemoryBuf buffer_; ///< fmt缓冲区
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_LOG_MSG_BUF_
