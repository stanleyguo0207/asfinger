#include "appender_console_color.h"

namespace afcore {

using namespace log;

template<typename Factory>
std::shared_ptr<CLogger> CorlorConsoleStdout_MT(const std::string &logger_name, EAppenderColorMode mode) {
  return Factory::template Create<RAppenderCorlorConsoleStdout_MT>(logger_name, mode);
}

template<typename Factory>
std::shared_ptr<CLogger> CorlorConsoleStdout_ST(const std::string &logger_name, EAppenderColorMode mode) {
  return Factory::template Create<RAppenderCorlorConsoleStdout_ST>(logger_name, mode);
}

template<typename Factory>
std::shared_ptr<CLogger> CorlorConsoleStderr_MT(const std::string &logger_name, EAppenderColorMode mode) {
  return Factory::template Create<RAppenderCorlorConsoleStderr_MT>(logger_name, mode);
}

template<typename Factory>
std::shared_ptr<CLogger> CorlorConsoleStderr_ST(const std::string &logger_name, EAppenderColorMode mode) {
  return Factory::template Create<RAppenderCorlorConsoleStderr_ST>(logger_name, mode);
}

template AFCORE_COMMON_API std::shared_ptr<CLogger> CorlorConsoleStdout_MT(const std::string &, EAppenderColorMode);
template AFCORE_COMMON_API std::shared_ptr<CLogger> CorlorConsoleStdout_ST(const std::string &, EAppenderColorMode);
template AFCORE_COMMON_API std::shared_ptr<CLogger> CorlorConsoleStderr_MT(const std::string &, EAppenderColorMode);
template AFCORE_COMMON_API std::shared_ptr<CLogger> CorlorConsoleStderr_ST(const std::string &, EAppenderColorMode);

} // !namespace afcore