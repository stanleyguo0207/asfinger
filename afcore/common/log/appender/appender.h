#ifndef AFCORE_COMMON_LOG_APPENDER_APPENDER_
#define AFCORE_COMMON_LOG_APPENDER_APPENDER_

#include "logcommon.h"
#include "log_msg.h"
#include "formater.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  附加器
class AFCORE_COMMON_API CAppender {
public:
  /// @brief  析构函数 默认
  virtual ~CAppender() = default;
  /// @brief  记录 纯虚
  /// @param  msg         日志信息
  virtual void Log(const SLogMessage& msg) = 0;
  /// @brief  刷新 纯虚
  virtual void Flush() = 0;
  /// @brief  设置模式 纯虚
  /// @param  pattern    模式
  virtual void SetPattern(const std::string& pattern) = 0;
  /// @brief  设置格式 纯虚
  /// @param  formatter   格式
  virtual void SetFormatter(std::unique_ptr<CFormatter> formatter) = 0;

  /// @brief  设置日志级别
  /// @param  l           要设置的日志最低级别
  void SetLevel(ELogLevel l);
  /// @brief  获取日志级别
  /// @return 当前日志级别
  [[nodiscard]] ELogLevel GetLevel() const;
  /// @brief  检查传入的日志级别是否需要记录
  /// @param  l           要检查的日志级别
  /// @return true 记录<br>
  ///         false 不记录
  [[nodiscard]] bool ShouldLog(ELogLevel l) const;
protected:
  std::atomic<uint8_t> level_ {kLogLevelTrace}; // 日志级别 默认是所有
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_APPENDER_APPENDER_