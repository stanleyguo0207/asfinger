#ifndef AFCORE_COMMON_LOG_APPENDER_APPENDER_BASE_
#define AFCORE_COMMON_LOG_APPENDER_APPENDER_BASE_

#include "logcommon.h"
#include "appender.h"
#include "log_msg.h"
#include "nocopyable.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  追加器父类
/// @tparam Mutex       互斥锁
template<typename Mutex>
class CAppenderBase
  : public CNocopyable
  , public CAppender {
public:
  /// @brief  默认构造函数
  CAppenderBase();
  /// @brief  显示构造函数
  /// @param  formatter       格式化类
  explicit CAppenderBase(RFormatterUptr formatter);

  /// @brief  记录
  /// @param  msg         日志信息
  void Log(const SLogMessage& msg) final;
  /// @brief  刷新
  void Flush() final;
  /// @brief  设置模式
  /// @param  parttern    模式
  void SetPattern(const std::string& parttern) final;
  /// @brief  设置格式
  /// @param  formatter   格式
  void SetFormatter(RFormatterUptr formatter) final;

protected:
  /// @brief  记录 子类实现
  /// @param  msg         日志信息
  virtual void DoLog(const SLogMessage& msg) = 0;
  /// @brief  刷新 子类实现
  virtual void DoFlush() = 0;
  /// @brief  设置模式 子类实现
  /// @param  parttern    模式
  virtual void DoSetPattern(const std::string& parttern);
  /// @brief  设置格式 子类实现
  /// @param  formatter   格式
  virtual void DoSetFormatter(RFormatterUptr formatter);

protected:
  RFormatterUptr formatter_;              ///< 格式化类
  Mutex mutex_;                           ///< 互斥锁
};

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_APPENDER_APPENDER_BASE_