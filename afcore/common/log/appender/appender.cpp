#include "appender.h"

namespace afcore {
namespace log {

void CAppender::SetLevel(ELogLevel l) {
  level_.store(l, std::memory_order_relaxed);
}

ELogLevel CAppender::GetLevel() const {
  return static_cast<ELogLevel>(level_.load(std::memory_order_relaxed));
}

bool CAppender::ShouldLog(ELogLevel l) const {
  return l >= level_.load(std::memory_order_relaxed);
}

} // !namespace log
} // !namespace afcore