#ifndef AFCORE_COMMON_LOG_APPENDER_APPENDER_CONSOLE_COLOR_
#define AFCORE_COMMON_LOG_APPENDER_APPENDER_CONSOLE_COLOR_

#if AFCORE_PLATFORM == AFCORE_PLATFORM_WIN
#include "appender_console_color_wincolor.h"
#else
#include "appender_console_color_ansi.h"
#endif

#include "sync.h"

namespace afcore {

/// @brief  日志
namespace log {

#if AFCORE_PLATFORM == AFCORE_PLATFORM_WIN
using RAppenderCorlorConsoleStdout_MT = RAppenderWincolorStdout_MT;
using RAppenderCorlorConsoleStdout_ST = RAppenderWincolorStdout_ST;
using RAppenderCorlorConsoleStderr_MT = RAppenderWincolorStderr_MT;
using RAppenderCorlorConsoleStderr_ST = RAppenderWincolorStderr_ST;
#else
using RAppenderCorlorConsoleStdout_MT = RAppenderAnsicolorStdout_MT;
using RAppenderCorlorConsoleStdout_ST = RAppenderAnsicolorStdout_ST;
using RAppenderCorlorConsoleStderr_MT = RAppenderAnsicolorStderr_MT;
using RAppenderCorlorConsoleStderr_ST = RAppenderAnsicolorStderr_ST;
#endif

} // !namespace log

using namespace log;

template<typename Factory = SSyncFactory>
std::shared_ptr<CLogger> CorlorConsoleStdout_MT(const std::string& logger_name,
  EAppenderColorMode mode = EAppenderColorMode::kAppenderColorMode_Automatic);

template<typename Factory = SSyncFactory>
std::shared_ptr<CLogger> CorlorConsoleStdout_ST(const std::string& logger_name,
  EAppenderColorMode mode = EAppenderColorMode::kAppenderColorMode_Automatic);

template<typename Factory = SSyncFactory>
std::shared_ptr<CLogger> CorlorConsoleStderr_MT(const std::string& logger_name,
  EAppenderColorMode mode = EAppenderColorMode::kAppenderColorMode_Automatic);

template<typename Factory = SSyncFactory>
std::shared_ptr<CLogger> CorlorConsoleStderr_ST(const std::string& logger_name,
  EAppenderColorMode mode = EAppenderColorMode::kAppenderColorMode_Automatic);

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_APPENDER_APPENDER_CONSOLE_COLOR_