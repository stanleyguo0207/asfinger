#include "appender_file.h"

#include <mutex>

namespace afcore {
namespace log {

template<typename Mutex>
CAppenderFile<Mutex>::CAppenderFile(const RFileName& filename, bool truncate) {
  helper_file_.Open(filename, truncate);
}

template<typename Mutex>
const RFileName &CAppenderFile<Mutex>::FileName() const {
  return helper_file_.FileName();
}

template<typename Mutex>
void CAppenderFile<Mutex>::DoLog(const SLogMessage &msg) {
  RMemoryBuf formatted;
  CAppenderBase<Mutex>::formatter_->Format(msg, formatted);
  helper_file_.Write(formatted);
}

template<typename Mutex>
void CAppenderFile<Mutex>::DoFlush() {
  helper_file_.Flush();
}

template class CAppenderFile<std::mutex>;
template class CAppenderFile<SNullMutex>;

} // !namespace log

using namespace log;

template<typename Factory>
std::shared_ptr<CLogger> File_MT(const std::string &logger_name, const RFileName &filename, bool truncate) {
  return Factory::template Create<RAppenderFile_MT>(logger_name, filename, truncate);
}

template<typename Factory>
std::shared_ptr<CLogger> File_ST(const std::string &logger_name, const RFileName &filename, bool truncate) {
  return Factory::template Create<RAppenderFile_ST>(logger_name, filename, truncate);
}

template AFCORE_COMMON_API std::shared_ptr<CLogger> File_MT(const std::string &, const RFileName &, bool);
template AFCORE_COMMON_API std::shared_ptr<CLogger> File_ST(const std::string &, const RFileName &, bool);

} // !namespace afcore