#ifndef AFCORE_COMMON_LOG_APPENDER_APPENDER_FILE_
#define AFCORE_COMMON_LOG_APPENDER_APPENDER_FILE_

#include <string>

#include "helper.h"
#include "helper_file.h"
#include "appender_base.h"
#include "sync.h"

namespace afcore {

/// @brief  日志
namespace log {

/// @brief  文件追加器
/// @tparam Mutex       互斥锁
template<typename Mutex>
class CAppenderFile final
  : public CAppenderBase<Mutex> {
public:
  explicit CAppenderFile(const RFileName& filename, bool truncate = false);
  [[nodiscard]] const RFileName& FileName() const;
protected:
  /// @brief  记录 子类实现
  /// @param  msg         日志信息
  void DoLog(const SLogMessage& msg) override;
  /// @brief  刷新 子类实现
  void DoFlush() override;
private:
  CHelperFile helper_file_; ///< 文件帮助类
};

using RAppenderFile_MT = CAppenderFile<std::mutex>;
using RAppenderFile_ST = CAppenderFile<SNullMutex>;

} // !namespace log

using namespace log;

template<typename Factory = SSyncFactory>
std::shared_ptr<CLogger> File_MT(const std::string& logger_name,
  const RFileName& filename,
  bool truncate = false);

template<typename Factory = SSyncFactory>
std::shared_ptr<CLogger> File_ST(const std::string& logger_name,
  const RFileName& filename,
  bool truncate = false);

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_APPENDER_APPENDER_FILE_