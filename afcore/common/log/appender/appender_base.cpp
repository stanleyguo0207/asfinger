#include "appender_base.h"

#include "formater_pattern.h"
#include "helper.h"

namespace afcore {
namespace log {

template<typename Mutex>
CAppenderBase<Mutex>::CAppenderBase()
  : formatter_(std::make_unique<CFormaterPattern>()) {

}

template<typename Mutex>
CAppenderBase<Mutex>::CAppenderBase(RFormatterUptr formatter)
  : formatter_(std::move(formatter)){

}

template<typename Mutex>
void CAppenderBase<Mutex>::Log(const SLogMessage& msg) {
  std::lock_guard<Mutex> lock(mutex_);
  DoLog(msg);
}

template<typename Mutex>
void CAppenderBase<Mutex>::Flush() {
  std::lock_guard<Mutex> lock(mutex_);
  DoFlush();
}

template<typename Mutex>
void CAppenderBase<Mutex>::SetPattern(const std::string& parttern) {
  std::lock_guard<Mutex> lock(mutex_);
  DoSetPattern(parttern);
}

template<typename Mutex>
void CAppenderBase<Mutex>::SetFormatter(RFormatterUptr formatter) {
  std::lock_guard<Mutex> lock(mutex_);
  DoSetFormatter(std::move(formatter));
}

template<typename Mutex>
void CAppenderBase<Mutex>::DoSetPattern(const std::string& parttern) {
  DoSetFormatter(std::make_unique<CFormaterPattern>(parttern));
}

template<typename Mutex>
void CAppenderBase<Mutex>::DoSetFormatter(RFormatterUptr formatter) {
  formatter_ = std::move(formatter);
}

template class CAppenderBase<std::mutex>;
template class CAppenderBase<SNullMutex>;

} // !namespace log
} // !namespace afcore