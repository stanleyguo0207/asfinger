#ifndef AFCORE_COMMON_LOG_LOG_FWD_
#define AFCORE_COMMON_LOG_LOG_FWD_

#include <memory>
#include <functional>

namespace afcore {

/// @brief  日志
namespace log {

struct SNullMutex;
struct SConsoleMutex;
struct SConsoleNullMutex;
struct SLogMessage;
class CLogMessageBuffer;
class CFormaterFlag;
class CFormatter;
class CFormaterPattern;
class CAppender;
class CLogger;
class CAppender;
class CAsyncLogger;
class CAsyncThreadPool;
class CPeriodicWorker;
class CLogBackTracer;

using RStringView = fmt::basic_string_view<char>;
using RWStringView = fmt::basic_string_view<wchar_t>;
using RMemoryBuf = fmt::basic_memory_buffer<char, 500>;
using RAppenderSptr = std::shared_ptr<CAppender>;
using RAppenderInitList = std::initializer_list<RAppenderSptr>;
using RErrHanlder = std::function<void(const std::string& err_msg)>;
using RLoggerSptr = std::shared_ptr<CLogger>;
using RAsyncLoggerSptr = std::shared_ptr<CAsyncLogger>;
using RAsyncThreadPoolWptr = std::weak_ptr<CAsyncThreadPool>;
using RAsyncThreadPoolSptr = std::shared_ptr<CAsyncThreadPool>;
using RFormaterFlagUptr = std::unique_ptr<CFormaterFlag>;
using RFormatterUptr = std::unique_ptr<CFormatter>;
using RPeriodicWorkerUptr = std::unique_ptr<CPeriodicWorker>;

} // !namespace log

} // !namespace afcore

#endif //! AFCORE_COMMON_LOG_LOG_FWD_