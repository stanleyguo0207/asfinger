#ifndef AFCORE_COMMON_ASIOWRAP_ASIOWRAP_IOCONTEXT_
#define AFCORE_COMMON_ASIOWRAP_ASIOWRAP_IOCONTEXT_

#include <boost/asio/io_context.hpp>
#include <boost/asio/post.hpp>

namespace afcore {
namespace asio {

class CIoContext {
public:
  CIoContext()
    : impl_() {}
  explicit CIoContext(int concurrency_hint)
    : impl_(concurrency_hint) {}

  operator boost::asio::io_context& () {
    return impl_;
  }
  operator const boost::asio::io_context& () const {
    return impl_;
  }

  size_t run() {
    return impl_.run();
  }
  void stop() {
    impl_.stop();
  };

  boost::asio::io_context::executor_type get_executor() noexcept {
    return impl_.get_executor();
  }

private:
  boost::asio::io_context impl_;
};

template<typename T>
inline decltype(auto) post(boost::asio::io_context& io_context, T&& t) {
  return boost::asio::post(io_context, std::forward<T>(t));
}

template<typename T>
inline decltype(auto) get_io_context(T&& io_object) {
  return io_object.get_executor().context();
}

} // !namespace asio
} // !namespace afcore

#endif //! AFCORE_COMMON_ASIOWRAP_ASIOWRAP_IOCONTEXT_