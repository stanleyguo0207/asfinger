#ifndef AFCORE_COMMON_ASIOWRAP_ASIOWRAP_FWD_
#define AFCORE_COMMON_ASIOWRAP_ASIOWRAP_FWD_

namespace boost {

namespace posix_time {

class ptime;

} // !namespace posix_time

namespace asio {

template <typename Time>
struct time_traits;

namespace ip {

class address;

class tcp;

template <typename InternetProtocol>
class basic_endpoint;

} // !namespace ip

class executor;

namespace ip {

template <typename InternetProtocol, typename Executor>
class basic_resolver;

using RTcpResolver = basic_resolver<tcp, executor>;

} // !namespace ip

} // !namespace asio

} // !namespace boost

namespace afcore {
namespace asio {

class CStrand;

} // !namespace asio
} // !namespace afcore

#endif //! AFCORE_COMMON_ASIOWRAP_ASIOWRAP_FWD_