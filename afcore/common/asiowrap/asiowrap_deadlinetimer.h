#ifndef AFCORE_COMMON_ASIOWRAP_ASIOWRAP_DEADLINETIMER_
#define AFCORE_COMMON_ASIOWRAP_ASIOWRAP_DEADLINETIMER_

#include <boost/asio/deadline_timer.hpp>

#define BasicDeadlineTimerThirdTemplateArg , boost::asio::io_context::executor_type

#define DeadlineTimerBase boost::asio::basic_deadline_timer<boost::posix_time::ptime, boost::asio::time_traits<boost::posix_time::ptime> BasicDeadlineTimerThirdTemplateArg>

namespace afcore {
namespace asio {

class CDeadlineTimer
  : public DeadlineTimerBase {
public:
  using DeadlineTimerBase::basic_deadline_timer;
};

} // !namespace asio
} // !namespace afcore

#endif //! AFCORE_COMMON_ASIOWRAP_ASIOWRAP_DEADLINETIMER_