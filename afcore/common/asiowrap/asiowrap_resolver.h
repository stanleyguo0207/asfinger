#ifndef AFCORE_COMMON_ASIOWRAP_ASIOWRAP_RESOLVER_
#define AFCORE_COMMON_ASIOWRAP_ASIOWRAP_RESOLVER_

#include <string>
#include <optional>

#include <boost/asio/ip/tcp.hpp>

namespace afcore {
namespace net {

inline std::optional<boost::asio::ip::tcp::endpoint> Resolve(boost::asio::ip::tcp::resolver& resolver,
  const boost::asio::ip::tcp& protocol,
  const std::string& host, const std::string& service) {
  boost::system::error_code ec;
  boost::asio::ip::tcp::resolver::results_type  results = resolver.resolve(protocol, host, service, ec);
  if (results.empty() || ec) {
    return {};
  }
  return results.begin()->endpoint();
}

} // !namespace net
} // !namespace afcore

#endif //! AFCORE_COMMON_ASIOWRAP_ASIOWRAP_RESOLVER_