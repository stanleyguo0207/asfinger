#ifndef AFCORE_COMMON_ASIOWRAP_ASIOWRAP_IPADDRESS_
#define AFCORE_COMMON_ASIOWRAP_ASIOWRAP_IPADDRESS_

#include <boost/asio/ip/address.hpp>

#include "define.h"

namespace afcore {
namespace net {

using boost::asio::ip::make_address;
using boost::asio::ip::make_address_v4;
using boost::asio::ip::make_address_v6;

inline uint32_t AddressV4ToUInt(const boost::asio::ip::address_v4& address) {
  return address.to_uint();
}

inline std::string AddressV4ToString(const boost::asio::ip::address_v4& address) {
  return address.to_string();
}

inline std::string AddressV6ToString(const boost::asio::ip::address_v6& address) {
  return address.to_string();
}

} // !namespace net
} // !namespace afcore

#endif //! AFCORE_COMMON_ASIOWRAP_ASIOWRAP_IPADDRESS_