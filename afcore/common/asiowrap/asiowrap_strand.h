#ifndef AFCORE_COMMON_ASIOWRAP_ASIOWRAP_SRAND_
#define AFCORE_COMMON_ASIOWRAP_ASIOWRAP_SRAND_

#include <boost/asio/strand.hpp>
#include <boost/asio/bind_executor.hpp>

#include "asiowrap_iocontext.h"

namespace afcore {
namespace asio {

class CStrand
  : public boost::asio::io_context::strand {
public:
  CStrand(CIoContext& io_context)
    : boost::asio::io_context::strand(io_context) {
  }
};

using boost::asio::bind_executor;

} // !namespace asio
} // !namespace afcore

#endif //! AFCORE_COMMON_ASIOWRAP_ASIOWRAP_SRAND_