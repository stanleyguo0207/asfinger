#ifndef AFCORE_COMMON_DEFINECOMPILER__
#define AFCORE_COMMON_DEFINECOMPILER__

#define AFCORE_PLATFORM_WIN   0
#define AFCORE_PLATFORM_UNIX  1

/// * platform
#if defined( _WIN64 )
# define AFCORE_PLATFORM AFCORE_PLATFORM_WIN
#elif defined( __WIN32__ ) || defined( WIN32 ) || defined( _WIN32 )
# define AFCORE_PLATFORM AFCORE_PLATFORM_WIN
#else
# define AFCORE_PLATFORM AFCORE_PLATFORM_UNIX
#endif

#define AFCORE_COMPILER_MICROSOFT 0
#define AFCORE_COMPILER_GNU       1

/// * compiler
#if defined( _MSC_VER )
# define AFCORE_COMPILER AFCORE_COMPILER_MICROSOFT
#elif defined( __GNUC__ )
# define AFCORE_COMPILER AFCORE_COMPILER_GNU
# define GCC_VERSION ( __GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__ )
#else
# error "FATAL ERROR: Unkown compiler."
#endif

#endif // !AFCORE_COMMON_DEFINECOMPILER__