#ifndef ASFCORE_COMMON_DEFINECXX__
#define ASFCORE_COMMON_DEFINECXX__

/// * __has_feature
#if defined(__has_feature)
# define AFCORE_HAS_FEATURE(x) __has_feature(x)
#else
# define AFCORE_HAS_FEATURE(x) 0
#endif

/// * __has_include
#if defined(__has_include) && !defined(__INTELLISENSE__) && \
    !(defined(__INTEL_COMPILER) && __INTEL_COMPILER < 1600)
# define AFCORE_HAS_INCLUDE(x) __has_include(x)
#else
# define AFCORE_HAS_INCLUDE(x) 0
#endif

/// * __has_cpp_attribute
#if defined(__has_cpp_attribute)
# define AFCORE_HAS_CPP_ATTRIBUTE(x) __has_cpp_attribute(x)
#else
# define AFCORE_HAS_CPP_ATTRIBUTE(x) 0
#endif

/// * override
#if AFCORE_HAS_FEATURE(cxx_override)
# define AFCORE_OVERRIDE override
#else
# define AFCORE_OVERRIDE
#endif

/// * [[noreturn]]
#if AFCORE_HAS_CPP_ATTRIBUTE(noreturn)
# define AFCORE_NORETURN [[noreturn]]
#else
# define AFCORE_NORETURN
#endif

/// * [[deprecated]]
#if AFCORE_HAS_CPP_ATTRIBUTE(deprecated)
# define AFCORE_DEPRECATED [[deprecated]]
#else
# define AFCORE_DEPRECATED
#endif

/// * string_view
#if AFCORE_HAS_INCLUDE(<string_view>)
# include <string_view>
# define AFCORE_USE_STRING_VIEW
#elif AFCORE_HAS_INCLUDE("experimental/string_view")
# include <experimental/string_view>
# define AFCORE_USE_EXPERIMENTAL_STRING_VIEW
#endif

#endif // !ASFCORE_COMMON_DEFINECXX__