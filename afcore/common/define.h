#ifndef AFCORE_COMMON_DEFINE__
#define AFCORE_COMMON_DEFINE__

#include <cstddef>
#include <cinttypes>
#include <climits>
#include <string>

#include "definecompiler.h"
#include "definecxx.h"

/// * gnu format macros
#if AFCORE_COMPILER == AFCORE_COMPILER_GNU
# if  !defined(__STDC_FORMAT_MACROS)
#   define __STDC_FORMAT_MACROS
# endif
# if  !defined(__STDC_CONSTANT_MACROS)
#   define __STDC_CONSTANT_MACROS
# endif
# if !defined(_GLIBCXX_USE_NANOSLEEP)
#   define _GLIBCXX_USE_NANOSLEEP
# endif
#endif

#define AFCORE_LITTLEENDIAN 0
#define AFCORE_BIGENDIAN    1

/// * net endian
#if !defined(AFCORE_ENDIAN)
# if defined (BOOST_BIG_ENDIAN)
#   define AFCORE_ENDIAN AFCORE_BIGENDIAN
# else
#   define AFCORE_ENDIAN AFCORE_LITTLEENDIAN
# endif
#endif

/// * windows 特殊处理
#if AFCORE_PLATFORM == AFCORE_PLATFORM_WIN
#  define AFCORE_PATH_MAX 260
#  define _USE_MATH_DEFINES
#  ifndef DECLSPEC_NORETURN
#    define DECLSPEC_NORETURN __declspec(noreturn)
#  endif
#  ifndef DECLSPEC_DEPRECATED
#    define DECLSPEC_DEPRECATED __declspec(deprecated)
#  endif
#else
#  define TRINITY_PATH_MAX PATH_MAX
#  define DECLSPEC_NORETURN
#  define DECLSPEC_DEPRECATED
#endif

/// * inline debug
#if !defined(AFCORE_COREDEBUG)
# define AFCORE_INLINE inline
#else
# if !defined(AFCORE_DEBUG)
#   define AFCORE_DEBUG
# endif
# define AFCORE_INLINE
#endif

/// * format check
#if !defined(AFCORE_FORMAT_CHECK_DISABLE)
# define AFCORE_FORMAT_CHECK (true)
#else
# define AFCORE_FORMAT_CHECK (false)
#endif

/// * gnu format
#if AFCORE_COMPILER == AFCORE_COMPILER_GNU
# define AFCORE_AF_NORETURN     __attribute__((__noreturn__))
# define AFCORE_AF_PRINTF(F, V) __attribute__ ((__format__ (__printf__, F, V)))
# define AFCORE_AF_DEPRECATED   __attribute__((__deprecated__))
#else
# define AFCORE_AF_NORETURN
# define AFCORE_AF_PRINTF(F, V)
# define AFCORE_AF_DEPRECATED
#endif

/// * dynamic
#if defined(AFCORE_API_USE_DYNAMIC_LINKING)
# if AFCORE_COMPILER == AFCORE_COMPILER_MICROSOFT
#   define AFCORE_API_EXPORT __declspec(dllexport)
#   define AFCORE_API_IMPORT __declspec(dllimport)
# elif AFCORE_COMPILER == AFCORE_PLATFORM_UNIX
#   define AFCORE_API_EXPORT __attribute__((visibility("default")))
#   define AFCORE_API_IMPORT
# else
#   error "FATAL ERROR: compiler not supported."
# endif
#else
# define AFCORE_API_EXPORT
# define AFCORE_API_IMPORT
#endif

/// * common api
#if defined(AFCORE_API_EXPORT_COMMON)
# define AFCORE_COMMON_API AFCORE_API_EXPORT
#else
# define AFCORE_COMMON_API AFCORE_API_IMPORT
#endif

/// * database api
#if defined(AFCORE_API_EXPORT_COMMON)
# define AFCORE_DATABASE_API AFCORE_API_EXPORT
#else
# define AFCORE_DATABASE_API AFCORE_API_IMPORT
#endif

/// * 64 %
#define UI64FMTD "%" PRIu64
#define UI64LIT(N) UINT64_C(N)

#define SI64FMTD "%" PRId64
#define SI64LIT(N) INT64_C(N)

#define SZFMTD "%" PRIuPTR

/// * eol definition
#if !defined(AFCORE_EOL)
# if AFCORE_PLATFORM == AFCORE_PLATFORM_WIN
#   define AFCORE_EOL "\r\n"
# else
#   define AFCORE_EOL "\n"
# endif
#endif
constexpr static const char* g_default_eol = AFCORE_EOL;

/// * folder separator
#if !defined(AFCORE_SEP)
# if AFCORE_PLATFORM == AFCORE_PLATFORM_WIN
#   define AFCORE_SEP '\\'
# else
#   define AFCORE_SEP '/'
# endif
#endif
constexpr static const char g_floder_sep = AFCORE_SEP;

/// @brief  文件名
using RFileName = std::string;

#endif // !AFCORE_COMMON_DEFINE__