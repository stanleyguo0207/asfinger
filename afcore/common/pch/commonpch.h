#ifndef AFCORE_COMMON_PCH__
#define AFCORE_COMMON_PCH__

#include <ctime>

#include <atomic>
#include <mutex>
#include <exception>
#include <string>
#include <vector>
#include <unordered_map>
#include <memory>
#include <chrono>
#include <functional>
#include <thread>
#include <condition_variable>
#include <type_traits>

#include <fmt/core.h>
#include <fmt/format.h>
#include <fmt/chrono.h>

#include "definecompiler.h"
#include "definecxx.h"
#include "define.h"
#include "common.h"
#include "copyable.h"
#include "nocopyable.h"

#include "config.h"
#include "duration.h"
#include "report.h"
#include "utils.h"

#include "logcommon.h"
#include "log_msg.h"
#include "log.h"

#endif // !AFCORE_COMMON_PCH__