#ifndef AFCORE_COMMON_BANNER_BANNER_
#define AFCORE_COMMON_BANNER_BANNER_

#include <functional>

#include "define.h"

namespace afcore {

namespace banner {

AFCORE_COMMON_API void Show(const char* app_name, std::function<void(const char*)> log, std::function<void()> extra);

AFCORE_COMMON_API void Show2(const char* app_name, std::function<void(const char*)> log, std::function<void()> extra);

} // !namespace banner

} // !namespace afcore

#endif //! AFCORE_COMMON_BANNER_BANNER_