#include "banner.h"

namespace afcore {
namespace banner {

void Show(const char* app_name, std::function<void(const char*)> log, std::function<void()> extra) {
  log("<Ctrl-C> to stop.");
  log(" ________  ________  ________ ___  ________   ________  _______   ________");
  log("|\\   __  \\|\\   ____\\|\\  _____\\\\  \\|\\   ___  \\|\\   ____\\|\\  ___ \\ |\\   __  \\");
  log("\\ \\  \\|\\  \\ \\  \\___|\\ \\  \\__/\\ \\  \\ \\  \\\\ \\  \\ \\  \\___|\\ \\   __/|\\ \\  \\|\\  \\");
  log(" \\ \\   __  \\ \\_____  \\ \\   __\\\\ \\  \\ \\  \\\\ \\  \\ \\  \\  __\\ \\  \\_|/_\\ \\   _  _\\");
  log("  \\ \\  \\ \\  \\|____|\\  \\ \\  \\_| \\ \\  \\ \\  \\\\ \\  \\ \\  \\|\\  \\ \\  \\_|\\ \\ \\  \\\\  \\|");
  log("   \\ \\__\\ \\__\\____\\_\\  \\ \\__\\   \\ \\__\\ \\__\\\\ \\__\\ \\_______\\ \\_______\\ \\__\\\\ _\\");
  log("    \\|__|\\|__|\\_________\\|__|    \\|__|\\|__| \\|__|\\|_______|\\|_______|\\|__|\\|__|");
  log("              \\|_________|                                                     ");
  log("email     stanleyguo0207@163.com");
  log("github    https://github.com/stanleyguo0207");
  log("gitee     https://gitee.com/stanleyguo0207");

  if (extra) {
    extra();
  }
}
void Show2(const char *app_name, std::function<void(const char *)> log, std::function<void()> extra) {
  log("██████████████████████████████████████████████████████████████████████");
  log("█   <Ctrl-C> to stop.                                                █");
  log("█                                                                    █");
  log("█    █████╗ ███████╗███████╗██╗███╗   ██╗ ██████╗ ███████╗██████╗    █");
  log("█   ██╔══██╗██╔════╝██╔════╝██║████╗  ██║██╔════╝ ██╔════╝██╔══██╗   █");
  log("█   ███████║███████╗█████╗  ██║██╔██╗ ██║██║  ███╗█████╗  ██████╔╝   █");
  log("█   ██╔══██║╚════██║██╔══╝  ██║██║╚██╗██║██║   ██║██╔══╝  ██╔══██╗   █");
  log("█   ██║  ██║███████║██║     ██║██║ ╚████║╚██████╔╝███████╗██║  ██║   █");
  log("█   ╚═╝  ╚═╝╚══════╝╚═╝     ╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝   █");
  log("█   email     stanleyguo0207@163.com                                 █");
  log("█   github    https://github.com/stanleyguo0207                      █");
  log("█   gitee     https://gitee.com/stanleyguo0207                       █");
  log("█                                                                    █");
  log("██████████████████████████████████████████████████████████████████████");

  if (extra) {
    extra();
  }
}



} // !namespace banner
} // !namespace afcore