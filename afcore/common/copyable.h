#ifndef AFCORE_COMMON_COPYABLE_
#define AFCORE_COMMON_COPYABLE_

namespace afcore {

class CCopyable {
protected:
  CCopyable() = default;
  ~CCopyable() = default;
};

} // !namespace afcore

#endif // !AFCORE_COMMON_COPYABLE_