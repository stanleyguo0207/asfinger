#ifndef AFCORE_COMMON_UTILITY_STRINGFORMAT_
#define AFCORE_COMMON_UTILITY_STRINGFORMAT_

#include <fmt/printf.h>

namespace afcore {

/// @brief  格式化字符串
/// @tparam Format      格式
/// @tparam ...Args     参数
/// @param  fmt         格式
/// @param  ...args     参数
/// @return 格式化后的字符串
template<typename Format, typename... Args>
inline std::string StringFormat(Format&& fmt, Args&&... args) {
  return fmt::sprintf(std::forward<Format>(fmt), std::forward<Args>(args)...);
}

/// @brief  判断格式化的字符串是否为空
/// @param  fmt         格式字符串
/// @return true        空
/// @return false       非空
inline bool IsFormatEmptyOrNull(const char* fmt)
{
  return fmt == nullptr;
}

/// @brief  判断格式化的字符串是否为空
/// @param  fmt         格式字符串
/// @return true        空
/// @return false       非空
inline bool IsFormatEmptyOrNull(std::string const& fmt)
{
  return fmt.empty();
}

} // !namespace afcore

#endif //! AFCORE_COMMON_UTILITY_STRINGFORMAT_