#ifndef AFCORE_COMMON_UTILITY_UTILS_
#define AFCORE_COMMON_UTILITY_UTILS_

#include <string>
#include <vector>

#include "define.h"
#include "nocopyable.h"

namespace afcore {

/// @brief  字符串转布尔值
/// @param  str     要转换的string
/// @return 转换后的布尔值
AFCORE_COMMON_API
bool StringToBool(std::string const& str);

/// @brief  去除左边空格
/// @param  [out]   src     原字符串
/// @return 去除左边空格后的字符串
AFCORE_COMMON_API
std::string& TrimLeft(std::string& src);
/// @brief  去除右边空格
/// @param  [out]   src     原字符串
/// @return 去除右边空格后的字符串
AFCORE_COMMON_API
std::string& TrimRight(std::string& src);
/// @brief  去除两边空格
/// @param  [out]   src     原字符串
/// @return 去除两边空格后的字符串
AFCORE_COMMON_API
std::string& Trim(std::string& src);

/// @brief  分词器
class AFCORE_COMMON_API CTokenizer
  : public CNocopyable {
public:
  using RStorage = std::vector<char const*>;        ///< 存储介质类型
  using RSizeType = RStorage::size_type;            ///< 存储介质大小类型
  using RConstIterator = RStorage::const_iterator;  ///<
  using RRefernce = RStorage::reference;            ///<
  using RConstRefernce = RStorage::const_reference; ///<
public:
  CTokenizer() = delete;
  /// @brief  分词器构造函数
  /// @param  src                 源字符串
  /// @param  delimiter           分隔符
  /// @param  reserve             预留大小
  /// @param  keep_empty_strings  保留空字符串标志
  explicit CTokenizer(const std::string& src, char const delimiter, uint32_t reserve = 0, bool keep_empty_strings = true);
  ~CTokenizer() { delete[] str_; }

  RConstIterator begin() const { return storage_.begin(); }
  RConstIterator end() const { return storage_.end(); }

  RSizeType size() const { return storage_.size(); }

  RRefernce operator [] (RSizeType i) { return storage_[i]; }
  RConstRefernce operator [] (RSizeType i) const { return storage_[i]; }

private:
  char* str_;         ///< 源字符串
  RStorage storage_;  ///< 分词后的存储器
};

} // !namespace afcore

#endif // !AFCORE_COMMON_UTILITY_UTILS_