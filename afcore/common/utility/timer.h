#ifndef AFCORE_COMMON_UTILITY_TIMER_
#define AFCORE_COMMON_UTILITY_TIMER_

#include "define.h"

#include <chrono>

#include "duration.h"

namespace afcore {

/// @brief  获取距离服务器启动后毫秒数时间差
/// @return 距离服务器启动后毫秒数时间差
inline uint32_t GetMsTimeFromAppStart() {
  static const RClockSteady::time_point s_app_start_time = RClockSteady::now();
  return static_cast<uint32_t>(std::chrono::duration_cast<RMilliseconds>(RClockSteady::now() - s_app_start_time).count());
}

/// @brief  获取两个毫秒时间的差值
/// @param  old_time        旧时间
/// @param  new_time        新时间
/// @param  时间差
inline uint32_t GetMsTimeDiff(uint32_t old_time, uint32_t new_time) {
  // 因为GetMsTimeFromAppStart限制了时间的范围
  if (old_time > new_time) {
    return (0xFFFFFFFF - old_time) + new_time;
  } else {
    return new_time - old_time;
  }
}

/// @brief  获取指定毫秒时间到现在的时间差
/// @param  old_time        旧的毫秒数
/// @reutrn 指定毫秒时间到现在的时间差
inline uint32_t GetMsTimeDiffToNow(uint32_t old_time) {
  return GetMsTimeDiff(old_time, GetMsTimeFromAppStart());
}
  
} // !namespace afcore

#endif // !AFCORE_COMMON_UTILITY_TIMER_