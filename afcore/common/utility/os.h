#ifndef AFCORE_COMMON_UTILITY_OS_
#define AFCORE_COMMON_UTILITY_OS_

#include "define.h"

namespace afcore {

/// @brief  获取进程id
/// @return 当前进程id
AFCORE_COMMON_API
int GetPid() noexcept;

/// @brief  获取线程id
/// @return 当前线程id
AFCORE_COMMON_API
size_t GetThreadId() noexcept;

/// @brief  是否在颜色的终端
/// @return 是否在颜色的终端
AFCORE_COMMON_API
bool IsColorTerminal() noexcept;

/// @brief  该文件是否为终端
/// @param  file        要检查的文件
/// @return 该文件是否为终端
AFCORE_COMMON_API
bool InTerminal(FILE* file) noexcept;

/// @brief  获取目录名称
/// @param  path        文件路径
/// @return 目录名称
AFCORE_COMMON_API
RFileName DirName(RFileName path);

/// @brief  路径是否存在
/// @param  filename    文件名 全路径
/// @return true        存在
/// @return false       不存在
AFCORE_COMMON_API
bool PathExists(const RFileName& filename) noexcept;

/// @brief  删除文件
/// @param  filename    要删除的文件
/// @return 成功返回1
AFCORE_COMMON_API
int Remove(const RFileName& filename) noexcept;

/// @brief  如果文件存在就删除
/// @param  filename    要删除的文件
/// @return 成功返回1
AFCORE_COMMON_API
int RemoveIfExists(const RFileName& filename) noexcept;

/// @brief  创建目录
/// @param  path        路径名
/// @return 创建是否成功
AFCORE_COMMON_API
bool CreateDir(RFileName path);

/// @brief  打开文件
/// @param  [out]   fp          文件句柄
/// @param  filename    文件名
/// @param  mode        文件打开模式
/// @return true        不成功
/// @return false       成功
AFCORE_COMMON_API
bool Fileopen(FILE** fp, const RFileName& filename, const RFileName& mode);

/// @brief  线程睡眠
/// @param  milliseconds        睡眠时间 单位毫秒
AFCORE_COMMON_API
void SleepMillis(int milliseconds) noexcept;

/// @brief  文件名转换
/// @param  filename            文件名
/// @return string类型的文件名
AFCORE_COMMON_API
std::string FileNameToString(const RFileName& filename);

/// @brief  获取文件大小
/// @param  f                   文件句柄
/// @return 文件大小
AFCORE_COMMON_API
size_t FileSize(FILE* f);

} // !namespace afcore

#endif //! AFCORE_COMMON_UTILITY_OS_