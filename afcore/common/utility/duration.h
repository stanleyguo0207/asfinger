#ifndef AFCORE_COMMON_UTILITY_DURATION_
#define AFCORE_COMMON_UTILITY_DURATION_

#include <chrono>

/// @brief  纳秒
using RNanoseconds = std::chrono::nanoseconds;
/// @brief  微妙
using RMicroseconds = std::chrono::microseconds;
/// @brief  毫秒
using RMilliseconds = std::chrono::milliseconds;
/// @brief  秒
using RSeconds = std::chrono::seconds;
/// @brief  分
using RMinutes = std::chrono::minutes;
/// @brief  小时
using RHours = std::chrono::hours;

/// @brief  使用用户自定义时间间隔
using namespace std::chrono_literals;

/// @brief  系统时钟
using RClockSystem = std::chrono::system_clock;
/// @brief  稳定时钟
using RClockSteady = std::chrono::steady_clock;
/// @brief  日志时钟
using RClockLog = std::chrono::system_clock;

/// @brief  获取给定时间点的秒数除外的毫秒部分
/// @tparam ToDuration      返还的时间单位
/// @param  tp              给定的时间点
/// @return 给定时间点的秒数除外的毫秒部分
template<typename ToDuration>
inline ToDuration TimeFraction(RClockLog::time_point tp) {
  auto duration = tp.time_since_epoch();
  auto secs = std::chrono::duration_cast<RSeconds>(duration);
  return std::chrono::duration_cast<ToDuration>(duration) - std::chrono::duration_cast<ToDuration>(secs);
}

#endif // !AFCORE_COMMON_UTILITY_DURATION_