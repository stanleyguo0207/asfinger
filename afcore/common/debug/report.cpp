#include "report.h"

#include <cstdio>
#include <cstdlib>
#include <cstdarg>

#include <thread>

namespace afcore {

void RptAssert(char const* file, int line, char const* function, char const* message) {
  fprintf(stderr, "\n%s:%d in %s ASSERTION FAILED:\n  %s\n", file, line, function, message);
  *((volatile int*)NULL) = 0;
  exit(1);
}

void RptAssert(char const* file, int line, char const* function, char const* message, char const* format, ...) {
  va_list args;
  va_start(args, format);

  fprintf(stderr, "\n%s:%d in %s ASSERTION FAILED:\n  %s ", file, line, function, message);
  vfprintf(stderr, format, args);
  fprintf(stderr, "\n");
  fflush(stderr);

  va_end(args);
  *((volatile int*)NULL) = 0;
  exit(1);
}

void RptFatal(char const* file, int line, char const* function, char const* message, ...) {
  va_list args;
  va_start(args, message);

  fprintf(stderr, "\n%s:%d in %s FATAL ERROR:\n  ", file, line, function);
  vfprintf(stderr, message, args);
  fprintf(stderr, "\n");
  fflush(stderr);

  std::this_thread::sleep_for(std::chrono::seconds(10));
  *((volatile int*)NULL) = 0;
  exit(1);
}

void RptError(char const* file, int line, char const* function, char const* message) {
  fprintf(stderr, "\n%s:%d in %s ERROR:\n  %s\n", file, line, function, message);
  *((volatile int*)NULL) = 0;
  exit(1);
}

void RptAbort(char const* file, int line, char const* function) {
  fprintf(stderr, "\n%s:%d in %s ABORTED.\n", file, line, function);
  *((volatile int*)NULL) = 0;
  exit(1);
}

void RptAbortHandler(int /*signal*/) {
  *((volatile int*)NULL) = 0;
  exit(1);
}

void RptWarning(char const* file, int line, char const* function, char const* message) {
  fprintf(stderr, "\n%s:%d in %s WARNING:\n  %s\n", file, line, function, message);
}

} // !namespace afcore