#ifndef AFCORE_COMMON_DEBUG_REPORT_
#define AFCORE_COMMON_DEBUG_REPORT_

#include "define.h"

namespace afcore {

AFCORE_NORETURN AFCORE_COMMON_API
void RptAssert(char const* file, int line, char const* function, char const* message);

AFCORE_NORETURN AFCORE_COMMON_API
void RptAssert(char const* file, int line, char const* function, char const* message, char const* format, ...) AFCORE_AF_PRINTF(5,6);

AFCORE_NORETURN AFCORE_COMMON_API
void RptFatal(char const* file, int line, char const* function, char const* message, ...) AFCORE_AF_PRINTF(4,5);

AFCORE_NORETURN AFCORE_COMMON_API
void RptError(char const* file, int line, char const* function, char const* message);

AFCORE_NORETURN AFCORE_COMMON_API
void RptAbort(char const* file, int line, char const* function);

AFCORE_NORETURN AFCORE_COMMON_API
void RptAbortHandler(int signal);

AFCORE_COMMON_API
void RptWarning(char const* file, int line, char const* function, char const* message);

} // !namespace afcore

#if AFCORE_COMPILER == AFCORE_COMPILER_MICROSOFT
#define ASSERT_BEGIN __pragma(warning(push)) __pragma(warning(disable: 4127))
#define ASSERT_END __pragma(warning(pop))
#else
#define ASSERT_BEGIN
#define ASSERT_END
#endif

#define DBG_ASSERT(cond, ...) ASSERT_BEGIN do { if (!(cond)) afcore::RptAssert(__FILE__, __LINE__, __FUNCTION__, #cond, ##__VA_ARGS__); } while(0)  ASSERT_END;
#define DBG_FATAL(cond, ...) ASSERT_BEGIN do { if (!(cond)) afcore::RptFatal(__FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__); } while(0)  ASSERT_END;
#define DBG_WARNING(cond, msg) ASSERT_BEGIN do { if (!(cond)) afcore::RptWarning(__FILE__, __LINE__, __FUNCTION__, (msg)); } while(0)  ASSERT_END;
#define DBG_ERROR(cond, msg) ASSERT_BEGIN do { if (!(cond)) afcore::RptError(__FILE__, __LINE__, __FUNCTION__, (msg)); } while(0)  ASSERT_END;
#define DBG_ABORT() ASSERT_BEGIN do { afcore::RptAbort(__FILE__, __LINE__, __FUNCTION__); } while(0)  ASSERT_END;

template<typename T>
inline T* ASSERT_NOTNULL_IMPL(T* ptr, char const* expr) {
  DBG_ASSERT(ptr, "%s", expr);
  return ptr;
}

#define DBG_ASSERT_NOTNULL(ptr) ASSERT_NOTNULL_IMPL(ptr, #ptr)

#endif // !AFCORE_COMMON_DEBUG_REPORT_