#ifndef AFCORE_DATABASE_ENV_DATABASE_ENV_
#define AFCORE_DATABASE_ENV_DATABASE_ENV_

#include "define.h"
#include "database_field.h"
#include "database_preparedstatement.h"
#include "database_query_callback.h"
#include "database_transaction.h"
#include "database_query_result.h"
#include "database_workerpool.h"

#include "connection_test.h"

namespace afcore {

using namespace database;

AFCORE_DATABASE_API extern CDatabaseWorkerPool<CTestConnection> g_database_test;

} // !namespace afcore

#endif //AFCORE_DATABASE_ENV_DATABASE_ENV_