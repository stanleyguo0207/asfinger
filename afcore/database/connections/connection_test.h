#ifndef AFCORE_DATABASE_CONNECTIONS_CONNECTION_TEST_
#define AFCORE_DATABASE_CONNECTIONS_CONNECTION_TEST_

#include "database_mysql_connection.h"

namespace afcore {

/// @brief  数据库
namespace database {

enum ETestDatabaseStatements : uint32_t {
  kTestDatabaseStatements_ALL_USER,

  kTestDatabaseStatements_Max
};

class AFCORE_DATABASE_API CTestConnection
  : public CMysqlConnection {
public:
  using RStatements = ETestDatabaseStatements;

  CTestConnection(SMysqlConnectionInfo& conn_info);
  CTestConnection(SMysqlConnectionInfo& conn_info, CPcQueue<CSqlOperation*>* queue);
  ~CTestConnection();

  void DoPreparedStatement() override;
};

} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_CONNECTIONS_CONNECTION_TEST_