#include "connection_test.h"

#include "database_preparedstatement_mysql.h"

namespace afcore {
namespace database {

CTestConnection::CTestConnection(SMysqlConnectionInfo& conn_info)
  : CMysqlConnection(conn_info) {
}

CTestConnection::CTestConnection(SMysqlConnectionInfo &conn_info, CPcQueue<CSqlOperation *> *queue)
  : CMysqlConnection(conn_info, queue) {
}

CTestConnection::~CTestConnection() {

}

void CTestConnection::DoPreparedStatement() {
  if (!reconnecting_) {
    stmts_.resize(kTestDatabaseStatements_Max);
  }

  PreparedStatement(kTestDatabaseStatements_ALL_USER, "SELECT * FROM USER", kConnectionFlags_Sync);
}

} // !namespace database
} // !namespace afcore