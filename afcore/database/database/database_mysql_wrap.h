#ifndef AFCORE_DATABASE_DATABASE_DATABASE_MYSQL_WRAP_
#define AFCORE_DATABASE_DATABASE_DATABASE_MYSQL_WRAP_

#include <type_traits>

#include "database_mysql_head.h"

namespace afcore {

struct SMysqlHandle : MYSQL {};
struct SMysqlResult : MYSQL_RES {};
struct SMysqlField : MYSQL_FIELD {};
struct SMysqlBind : MYSQL_BIND {};
struct SMysqlStmt : MYSQL_STMT {};

using RMysqlBool = std::remove_pointer_t<decltype(std::declval<MYSQL_BIND>().is_null)>;

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_MYSQL_WRAP_