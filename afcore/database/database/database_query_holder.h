#ifndef AFCORE_DATABASE_DATABASE_DATABASE_QUERY_HOLDER_
#define AFCORE_DATABASE_DATABASE_DATABASE_QUERY_HOLDER_

#include <vector>

#include "database_sql_op.h"

namespace afcore {

/// @brief  数据库
namespace database {

class AFCORE_DATABASE_API CSqlQueryHolderBase {
  friend class CSqlQueryHolderTask;
public:
  CSqlQueryHolderBase() = default;
  virtual ~CSqlQueryHolderBase();

  void SetSize(size_t size);
  RPreparedQueryResultSptr GetPreparedResult(size_t index);
  void SetPreparedResult(size_t index, CPreparedResultSet* result);
protected:
  bool SetPreparedQueryImpl(size_t index, CPreparedStatementBase* stmt);
private:
  std::vector<std::pair<CPreparedStatementBase*, RPreparedQueryResultSptr>> queries_;
};

template<typename T>
class CSqlQueryHolder
  : public CSqlQueryHolderBase {
public:
  bool SetPreparedQuery(size_t index, CPreparedStatementBase* stmt) {
    return SetPreparedQueryImpl(index, stmt);
  }
};

class AFCORE_DATABASE_API CSqlQueryHolderTask
  : public CSqlOperation {
public:
  CSqlQueryHolderTask(CSqlQueryHolderBase* holder)
    : holder_(holder) {
  }
  ~CSqlQueryHolderTask();

  bool Execute() override;
  RQueryHolderFutrue GetFuture() { return result_.get_future(); }
private:
  CSqlQueryHolderBase* holder_{nullptr};
  RQueryHolderPromise result_;
  bool executed_{false};
};

} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_QUERY_HOLDER_