#include "database_query_process.h"

#include <algorithm>

#include "database_query_callback.h"

namespace afcore {
namespace database {

CQueryCallbackProcessor::CQueryCallbackProcessor() {

}

CQueryCallbackProcessor::~CQueryCallbackProcessor() {

}

void CQueryCallbackProcessor::AddQuery(CQueryCallback&& callback) {
  callbacks_.emplace_back(std::move(callback));
}

void CQueryCallbackProcessor::ProcessReadyQueries() {
  if (callbacks_.empty()) {
    return;
  }

  /// 这么做的原因是因为多线程的环境中 处理此时此刻的回调列表
  std::vector<CQueryCallback> update_callbacks {std::move(callbacks_)};

  update_callbacks.erase(std::remove_if(update_callbacks.begin(), update_callbacks.end(),
    [](CQueryCallback& callback) {
      return callback.InvokeIfReady() == CQueryCallback::kStatus_Completed;
    }), update_callbacks.end());

  callbacks_.insert(callbacks_.end(), std::make_move_iterator(update_callbacks.begin()), std::make_move_iterator(update_callbacks.end()));
}

} // !namespace database
} // !namespace afcore