#ifndef AFCORE_DATABASE_DATABASE_DATABASE_ADHOCSTATEMENT_
#define AFCORE_DATABASE_DATABASE_DATABASE_ADHOCSTATEMENT_

#include "define.h"
#include "database_fwd.h"
#include "database_sql_op.h"

namespace afcore {

/// @brief  数据库
namespace database {

class AFCORE_DATABASE_API CBasicStatementTask
  : public CSqlOperation {
public:
  CBasicStatementTask(const char* sql, bool async = false);
  ~CBasicStatementTask();

  bool Execute() override;
  [[nodiscard]] RQueryResultFutrue GetFuture() const { return result_->get_future(); }
private:
  const char* sql_{nullptr};
  bool has_result_{false};
  RQueryResultPromise* result_{nullptr};
};

} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_ADHOCSTATEMENT_