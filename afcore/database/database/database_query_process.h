#ifndef AFCORE_DATABASE_DATABASE_DATABASE_QUERY_PROCESS_
#define AFCORE_DATABASE_DATABASE_DATABASE_QUERY_PROCESS_

#include <vector>

#include "define.h"
#include "database_fwd.h"

namespace afcore {

/// @brief  数据库
namespace database {

class AFCORE_DATABASE_API CQueryCallbackProcessor
  : public CNocopyable {
public:
  CQueryCallbackProcessor();
  ~CQueryCallbackProcessor();

  void AddQuery(CQueryCallback&& callback);
  void ProcessReadyQueries();
private:
  std::vector<CQueryCallback> callbacks_;
};

} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_QUERY_PROCESS_