#ifndef AFCORE_DATABASE_DATABASE_DATABASE_WORKER_
#define AFCORE_DATABASE_DATABASE_DATABASE_WORKER_

#include <atomic>
#include <thread>

#include "define.h"
#include "nocopyable.h"

namespace afcore {

template<typename T>
class CPcQueue;

/// @brief  数据库
namespace database {

class CMysqlConnection;
class CSqlOperation;

class AFCORE_DATABASE_API CDatabaseWorker
  : public CNocopyable {
public:
  CDatabaseWorker(CPcQueue<CSqlOperation*>* new_queue, CMysqlConnection* conn);
  ~CDatabaseWorker();
private:
  void WorkerThread();
private:
  CPcQueue<CSqlOperation*>* queue_ {nullptr};
  CMysqlConnection* conn_ {nullptr};
  std::thread worker_thread_;
  std::atomic<bool> cancel_action_token_ {false};
};


} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_WORKER_