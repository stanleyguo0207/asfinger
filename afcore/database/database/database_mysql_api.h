#ifndef AFCORE_DATABASE_DATABASE_MYSQL_API_
#define AFCORE_DATABASE_DATABASE_MYSQL_API_

#include "define.h"

namespace afcore {

/// @brief  数据库
namespace database {

/// @brief  mysql
namespace mysql {

/// @brief  mysql初始化
AFCORE_DATABASE_API void LibraryInit();

/// @brief  mysql关闭
AFCORE_DATABASE_API void LibraryEnd();

/// @brief  获取mysql的版本信息
/// @return mysql的版本信息
AFCORE_DATABASE_API const char* GetLibraryVersion();

} // !namespace mysql

} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_MYSQL_API_