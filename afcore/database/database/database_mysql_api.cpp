#include "database_mysql_api.h"

#include "database_mysql_head.h"

namespace afcore {
namespace database {
namespace mysql {

void LibraryInit() {
  mysql_library_init(-1, nullptr, nullptr);
}

void LibraryEnd() {
  mysql_library_end();
}

const char* GetLibraryVersion() {
  return MYSQL_SERVER_VERSION;
}

} // !namespace mysql
} // !namespace database
} // !namespace afcore