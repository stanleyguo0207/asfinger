#ifndef AFCORE_DATABASE_DATABASE_DATABASE_FWD_
#define AFCORE_DATABASE_DATABASE_DATABASE_FWD_

#include <future>
#include <memory>

namespace afcore {

/// @brief  数据库
namespace database {

class CField;

class CResultSet;
using RQueryResultSptr = std::shared_ptr<CResultSet>;
using RQueryResultFutrue = std::future<RQueryResultSptr>;
using RQueryResultPromise = std::promise<RQueryResultSptr>;

class CPreparedResultSet;
using RPreparedQueryResultSptr = std::shared_ptr<CPreparedResultSet>;
using RPreparedQueryResultFutrue = std::future<RPreparedQueryResultSptr>;
using RPreparedQueryResultPromise = std::promise<RPreparedQueryResultSptr>;

class CPreparedStatementBase;

template<typename T>
class CPreparedStatement;

class CQueryCallback;

class CTransactionBase;

template<typename T>
class CTransaction;

template<typename T>
using RSqlTransaction = std::shared_ptr<CTransaction<T>>;

class CSqlQueryHolderBase;
using RQueryHolderFutrue = std::future<CSqlQueryHolderBase*>;
using RQueryHolderPromise = std::promise<CSqlQueryHolderBase*>;

template<typename T>
class CSqlQueryHolder;

} // !namespace database

struct SMysqlHandle;
struct SMysqlResult;
struct SMysqlField;
struct SMysqlBind;
struct SMysqlStmt;

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_FWD_