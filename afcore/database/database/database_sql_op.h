#ifndef AFCORE_DATABASE_DATABASE_DATABASE_SQL_OP_
#define AFCORE_DATABASE_DATABASE_DATABASE_SQL_OP_

#include "define.h"
#include "database_fwd.h"
#include "nocopyable.h"

namespace afcore {

/// @brief  数据库
namespace database {

/// @brief  存储sql对象数据
union USqlElementUion {
  CPreparedStatementBase* stmt; ///< 预处理语句
  const char* query;            ///< 原始查询
};

/// @brief  sql对象数据类型
enum ESqlElementDataType {
  kSqlElementDataType_Raw,      ///< 原始
  kSqlElementDataType_Prepared  ///< 预处理
};

/// @brief  sql对象数据
struct SSqlElementData {
  USqlElementUion element;      ///< 对象数据
  ESqlElementDataType type;     ///< 对象数据类型
};

/// @brief  模糊结果集
union USqlResultSetUnion {
  CResultSet* result;           ///< 原生查询结果
  CPreparedResultSet* presult;  ///< 准备查询结果
};

class CMysqlConnection;

/// @brief  sql操作
class AFCORE_DATABASE_API CSqlOperation
  : public CNocopyable {
public:
  /// @brief  构造函数
  CSqlOperation() = default;
  /// @brief  析构函数
  virtual ~CSqlOperation() {}

  /// @brief  执行接口
  virtual int Call() {
    Execute();
    return 0;
  }

  /// @brief  执行操作 纯虚
  /// @return 执行结果
  virtual bool Execute() = 0;
  /// @brief  设置mysql连接 纯虚
  /// @param  conn  mysql连接
  virtual void SetConnection(CMysqlConnection* conn) { conn_ = conn; }

  CMysqlConnection* conn_ {nullptr};  ///< mysql数据库连接
};

} // !namespace database

} // !namespace afcore

#endif //! AFCORE_DATABASE_DATABASE_DATABASE_SQL_OP_