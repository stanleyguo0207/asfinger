#ifndef AFCORE_DATABASE_PCH__
#define AFCORE_DATABASE_PCH__

#include <string>
#include <vector>

#if AFCORE_PLATFORM == AFCORE_PLATFORM_WIN
#include <winsock2.h>
#endif
#include <mysql.h>

#include "definecompiler.h"
#include "definecxx.h"
#include "define.h"
#include "common.h"
#include "copyable.h"
#include "nocopyable.h"

#include "log.h"

#endif //! AFCORE_DATABASE_PCH__