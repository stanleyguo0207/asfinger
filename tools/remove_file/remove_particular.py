import os

def del_files(path):
  for root, dirs, files in os.walk(path):
    for name in files:
      if(name.endswith(".proto")):
        os.remove(os.path.join(root, name))
        print("Delete File: " + os.path.join(root, name))

if __name__ == "__main__":
  path = './'
  del_files(path)