import time
import os

all_path = []

def GetAllfile(path):
  all_file_list = os.listdir(path)
  for file in all_file_list:
    file_path = os.path.join(path, file)
    if os.path.isdir(file_path):
      all_path.append(file_path)
      GetAllfile(file_path)
    elif os.path.isfile(file_path):
      all_path.append(file_path)

if __name__ == "__main__":
  path = './'
  now = time.time()
  GetAllfile(path)
  for file in all_path:
    os.utime(file, (now, now))