import time
import os

path = './'
now = time.time()
formater = "%d.%m.%Y %H:%M:%S"
all_path = []

def IsFuture(file_name):
  mtime = time.strftime(formater, time.localtime(os.path.getmtime(file_name)))
  atime = time.strftime(formater, time.localtime(os.path.getatime(file_name)))
  mtime_t = time.mktime(time.strptime(mtime, formater))
  atime_t = time.mktime(time.strptime(atime, formater))
  return mtime_t > now or atime_t > now

def GetAllfile(path):
  all_file_list = os.listdir(path)
  for file in all_file_list:
    file_path = os.path.join(path, file)
    if os.path.isdir(file_path):
      if IsFuture(file_path):
        all_path.append(file_path)
      GetAllfile(file_path)
    elif os.path.isfile(file_path):
      if IsFuture(file_path):
        all_path.append(file_path)

if __name__ == "__main__":
  GetAllfile(path)
  for file in all_path:
    os.utime(file, (now, now))